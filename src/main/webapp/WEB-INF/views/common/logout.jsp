<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="/resources/js/vendor/jquery-1.12.1.min.js"></script>
<script>
	localStorage.clear();
	sessionStorage.clear();
	location.href="/";
</script>