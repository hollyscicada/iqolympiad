<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="/resources/js/vendor/jquery-1.12.1.min.js"></script>
<script src="/resources/js/commonAjax.js?v=${nowDate}"></script>
<script>
	$(function(){
		var result_seq = sessionStorage.getItem("result_seq");
		var authToken = "${sessionScope.MEMBER.authToken}";
		
		if(result_seq && authToken){
			var param = {
					"result_seq":result_seq
			}
			ajaxCallPostToken("/api/v1/iq/test/save/update", authToken, param, function(res){
				if(res.success){
					alert("Your IQ Test result has been saved.");
					sessionStorage.removeItem("result_seq");
					initSuccess(res);
				}else{
					initFail();
				}
			})
		}else{
			initFail();
		}
	})

	function init(res){
		var custom_type = "${sessionScope.MEMBER.custom_type}"
		var custom_code = "${sessionScope.MEMBER.custom_code}"
		localStorage.setItem("custom_type", custom_type);
		localStorage.setItem("custom_code", custom_code);
		
	}
	function initSuccess(res){
		init();
		if(res.redirectType == 'RESULT'){
			location.href="/TestsResult?result_seq="+res.data.result_seq;
		}else{
			
			if(res.data && res.data.event_yn && res.data.event_yn == 'Y'){
				location.href="/TestsResultEvent?result_seq="+res.data.result_seq;
			}else{
				location.href="/";
			}
			
		}
	}
	function initFail(){
		init();
		location.href="/";
	}
</script>