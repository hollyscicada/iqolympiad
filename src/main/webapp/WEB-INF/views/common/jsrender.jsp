<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="https://www.jsviews.com/download/jsrender.js"></script>
<script>
$.views.converters("status",
        function(status_code) {
		  var status_text = "";
		  if(status_code == '0'){
			  status_text = status_text0;
		  }else if(status_code == '1'){
			  status_text = status_text1;
		  }
          return status_text;
        }
      );
$.views.converters("dec",
        function(val) {
          return global.comma(val);
        }
      );
$.views.converters("eval",
        function(val) {
          return val;
        }
      );
      
$.views.helpers("hashtag",
        function(val) {
			if(val){
				var hashtags = val.split("|");
				var hashTagList = [];
				for(var i = 0 ; i < hashtags.length ; i++){
					var param = {
							"name":hashtags[i]
					}
					hashTagList.push(param);
					
				}
				return hashTagList;
			}
			return null;
        }
      );
$.views.converters("view",
        function(val) {
	 		var content = val.replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&#039;/g,"'").replace(/&quot;/g,"\"");
	   		return content;
        }
      );
$.views.converters("calChk",
        function(val) {
			if(val%4==0){
				return true
			}else{
		   		return false;
			}
        }
      );
$.views.converters("changeEng",
        function(val) {
			if(val == 0){
				return "a";
			}
			if(val == 1){
				return "b";
			}
			if(val == 2){
				return "c";
			}
			if(val == 3){
				return "d";
			}
			if(val == 4){
				return "e";
			}
			if(val == 5){
				return "f";
			}
        }
      );
$(function(){
	$(document).on("click", ".detail-tournament-btn", function(){
		location.href="/tournament/"+$(this).attr("data-seq");
	})
})
</script>
<script type="text/x-jsrender" id="countryList">
		<option value="">Select</option>
		{{for data }}
			<option value="{{:country_seq}}">{{:name}}({{:name_code}})</option>
		{{/for}}
</script>
<script type="text/x-jsrender" id="actList">
		{{for data }}
			<div class="col-lg-3 col-sm-6 tp-li iq-test-item" data-seq="{{:test_seq}}">
					<div class="tp-li-inner">
						<div class="tp-img">
							<img src="{{:img_url}}" onerror="this.src='/resources/img/tpImg.png'" alt="img" />
						</div>
						<div class="tp-txt">
							<h4>
								<b>{{:title}}</b><span class="ic-id" style="background-image:url('{{:write_img_url}}'), url('/resources/img/user_img.png');background-size: 18px auto;background-position: left center;">{{:write_name}}</span>
							</h4>
							<p>
								<span class="ic-down">{{:test_cnt}}</span> <span class="ic-time">{{:ing_time}}</span>
							</p>
						</div>
					</div>
				</div>
		{{/for}}
</script>
<script type="text/x-jsrender" id="uctList">
		{{for data }}
				<div class="col-lg-3 col-sm-6 uct-li iq-test-item" data-seq="{{:test_seq}}">
					<div class="uct-li-inner">
						<div class="uct-img">
							<img src="{{:img_url}}" onerror="this.src='/resources/img/uctImg.jpg'"  alt="img" />
						</div>
						<div class="uct-txt">
							<h4>
								<b>{{:title}}</b><span class="ic-id" style="background-image:url('{{:write_img_url}}'), url('/resources/img/user_img.png');background-size: 18px auto;background-position: left center;">{{:write_name}}</span>
							</h4>
							<p>
								<span class="ic-down">{{:test_cnt}}</span> <span class="ic-time">{{:ing_time}}</span> <span class="ic-star">0.0k</span>
							</p>
						</div>
					</div>
				</div>
		{{/for}}
</script>
<script type="text/x-jsrender" id="recommendList">
		{{for recommend_list }}
								<div class="col-lg-4 col-sm-6 uct-li iq-test-item swiper-slide" data-seq="{{:test_seq}}">
									<div class="uct-li-inner">
										<div class="uct-img">
											<img src="{{:img_url}}" onerror="this.src='/resources/img/uctImg.jpg'" alt="img" />
										</div>
										<div class="uct-txt">
											<h4>
												<b>{{:title}}</b><span class="ic-id" style="background-image:url('{{:write_img_url}}'), url('/resources/img/user_img.png');background-size: 18px auto;background-position: left center;">{{:write_name}}</span>
											</h4>
											<p>
												<span class="ic-down">{{:test_cnt}}</span> <span class="ic-time">{{:ing_time}}</span> <span class="ic-star">0.0k</span>
											</p>
										</div>
									</div>
								</div>
		{{/for}}
</script>
<script type="text/x-jsrender" id="rankingTbody">
<tr>
				<td>1</td>
				<td>40</td>


				<td>11</td>
				<td>52</td>


				<td>21</td>
				<td>92</td>


				<td>31</td>
				<td>132</td>
</tr>
<tr>
				<td>2</td>
				<td>40</td>


				<td>12</td>
				<td>56</td>


				<td>22</td>
				<td>96</td>


				<td>32</td>
				<td>136</td>
</tr>
<tr>

				<td>3</td>
				<td>40</td>


				<td>13</td>
				<td>60</td>


				<td>23</td>
				<td>100</td>


				<td>33</td>
				<td>140</td>
</tr>
<tr>

				<td>4</td>
				<td>40</td>


				<td>14</td>
				<td>64</td>


				<td>24</td>
				<td>104</td>


				<td>34</td>
				<td>144</td>

</tr>
<tr>
				<td>5</td>
				<td>40</td>


				<td>15</td>
				<td>68</td>


				<td>25</td>
				<td>108</td>


				<td>35</td>
				<td>148</td>

</tr>
<tr>
				<td>6</td>
				<td>40</td>


				<td>16</td>
				<td>72</td>


				<td>26</td>
				<td>112</td>

				<td>36</td>
				<td>152</td>

</tr>
<tr>
				<td>7</td>
				<td>40</td>


				<td>17</td>
				<td>76</td>


				<td>27</td>
				<td>116</td>


				<td>37</td>
				<td>156</td>
</tr>
<tr>

				<td>8</td>
				<td>40</td>


				<td>18</td>
				<td>80</td>


				<td>28</td>
				<td>120</td>


				<td>38</td>
				<td>160</td>
</tr>
<tr>

				<td>9</td>
				<td>44</td>


				<td>19</td>
				<td>84</td>


				<td>29</td>
				<td>124</td>



				<td>39</td>
				<td>160</td>

</tr>
<tr>
				<td>10</td>
				<td>48</td>


				<td>20</td>
				<td>88</td>


				<td>30</td>
				<td>128</td>

				<td>40</td>
				<td>160</td>
</tr>

</script>
<script type="text/x-jsrender" id="rankingTbodyEvent">
<tr>
				<td>0</td>
				<td>80</td>


				<td>3</td>
				<td>95</td>


				<td>6</td>
				<td>120</td>


				<td>9</td>
				<td>150</td>
</tr>
<tr>
				<td>1</td>
				<td>85</td>


				<td>4</td>
				<td>100</td>


				<td>7</td>
				<td>130</td>

				<td>10</td>
				<td>160</td>
</tr>
<tr>

				<td>2</td>
				<td>90</td>


				<td>5</td>
				<td>110</td>


				<td>8</td>
				<td>140</td>

</tr>

</script>
<script type="text/x-jsrender" id="rankingTbodyMember">
		{{for ranking_list }}
				{{if #getIndex() convert='calChk' }}
					</tr>
				{{/if}}
				{{if #getIndex() convert='calChk' }}
					<tr>
				{{/if}}
					<td>{{:rownum}}</td>
					<td>{{:score}}</td>
		{{/for}}
</script>
<script type="text/x-jsrender" id="historyList">
		{{for history_list }}
				<li>{{:create_dt}} - {{:content}}</li>
		{{/for}}
</script>
<script type="text/x-jsrender" id="newList">
		{{for data }}
								<li>
									<h4>
										<b>{{:title}}</b><span>{{:create_dt}} · {{if left_day }}{{:left_day}} day{{/if}} {{:left_hour}} hours left</span>
									</h4> <a data-test-seq="{{:test_seq}}" data-result-seq="{{:result_seq}}" class="btn-{{if is_restart == '1'}}inprogress{{else is_restart == '0'}}finished{{/if}}">{{if is_restart == '1'}}In Progress{{else is_restart == '0'}}Finished{{/if}}</a>
								</li>
		{{/for}}
</script>
<script type="text/x-jsrender" id="myList">
		{{for data }}
									<tr>
										<td>{{:title}}</td>
										<td>{{:score}}</td>
										<td>{{:success_cnt}}/{{:question_cnt}}</td>
										<td>{{:test_cnt}}</td>
										<td>{{:type}}</td>
										<td>{{:create_dt}}</td>
										<td class="comingsoon-btn"><a class="btn-share"></a></td>
									</tr>
		{{/for}}
</script>
<script type="text/x-jsrender" id="rankingList">
		{{for data }}
					<tr>
										<td>{{:rownum}}</td>
										<td>
											{{if change_type == 'UP'}}
												<span class="up">{{:change_ranking}}</span>
											{{else change_type == 'DOWN'}}
												<span class="down">{{:change_ranking}}</span>
											{{else change_type == 'CHK'}}
												<span>-</span>
											{{/if}}
										</td>
										<td><span class="user-li"><b class="user-img"><img src="{{:img_url}}" onerror="this.src='/resources/img/user_img.png'" alt="logo"></b>{{:name}}</span></td>
										<td>{{:iq}}</td>
										<td>{{:test_cnt}} ({{:question_cnt}})</td>
										<td class="mobile-hide">{{:country_name_code}}</td>
									</tr>
		{{/for}}
</script>
<script type="text/x-jsrender" id="questionStatus">
		{{for question_list }}
			{{if solve_yn == 'Y'}}
				<a data-que-index="{{:#getIndex()}}" class="complete footer-question-sta">{{:#getIndex()+1}}</a>
			{{else}}
				<a data-que-index="{{:#getIndex()}}" class="footer-question-sta">{{:#getIndex()+1}}</a>
			{{/if}}
		{{/for}}
</script>
<script type="text/x-jsrender" id="question_list">
		{{for question_list }}
					<div class="col-md-6 test-li testQ question-dom-{{:#getIndex()}}" style="display:none">
						{{if question_type == 'IMG'}}
							<img src="{{:question_img_url}}" alt="img" style="display: inline-block; width:50%;" />
						{{else}}
							<span>{{:question}}</span>
						{{/if}}
					</div>
					<div class="col-md-6 test-li testA question-dom-{{:#getIndex()}}" style="display:none">
						<ul>
							{{for example_list}}
								<li data-example-seq="{{:example_seq}}" data-question-seq="{{:question_seq}}" class="input-li">
										<input id="testA_a_{{:example_seq}}_{{:#getIndex()}}" type="radio" class="testa_input" name="testa_{{:#parent.getIndex()}}" style="display:none" {{if choice_yn == 'Y'}}checked="checked"{{/if}} /> 
										<label for="testA_a_{{:example_seq}}_{{:#getIndex()}}" class="testa">
											<span>
												{{changeEng:#getIndex()}}.
													{{if example_type == 'IMG'}}
														<b class="example-chk"><img src="{{:example_img_url}}"></b>
													{{else}}
														<b class="example-chk">{{:example}}</b>
													{{/if}}
											</span>
										</label>
									</li>
							{{/for}}
						</ul>
					</div>
		{{/for}}
</script>
<script type="text/x-jsrender" id="categoryList">
		{{for data }}
					<div class="gragh-li">
						<span class="img"><img src="{{:img_url}}"></span>
						<span class="range"><input type="range" disabled="disabled"  class="category-test-score" min="0" max="200" value="{{if score }}{{:score}}{{else}}0{{/if}}"></span>
						<span class="num">
							{{if score }}
								{{:score}}
							{{else}}
								N/A
							{{/if}}
						</span>
						<span class="category-info-span"><span class="category-name-span">{{:title}}</span><span class="test-cnt-span">{{if test_cnt}}  {{:test_cnt}} tests completed{{/if}}</span></span>
					</div>
		{{/for}}
</script>
<script type="text/x-jsrender" id="bestRankingList">
		{{for best_ranking_list }}
					<div class="col-md-4">
									<ul class="rank">
										<li class="rank-img"><span><img src="/resources/img/ic-awards-0{{:#index+1}}.svg" alt="img" /></span></li>
										<li class="rank-txt">
											<h4>
												{{:name}}<br> <span>Raw {{:success_cnt}}/{{:question_cnt}} ({{:test_cnt}} nd Attempt)</span> <span>IQ : {{:score}} (SD 15)</span>
											</h4>
										</li>
									</ul>
								</div>
		{{/for}}
</script>
<script type="text/x-jsrender" id="jobList">
		{{for jobList }}
				<li><span style="cursor:unset">{{:name}}</span></li>
		{{/for}}
</script>
<script type="text/x-jsrender" id="ageList">
		<option value="">Select</option>
		<option value="1">1~10 age</option>
		<option value="2">11~20 age</option>
		<option value="3">21~30 age</option>
		<option value="4">31~40 age</option>
		<option value="5">41~50 age</option>
		<option value="6">51~60 age</option>
		<option value="7">61~70 age</option>
		<option value="8">71~80 age</option>
		<option value="9">81~90 age</option>
		<option value="10">91~100 age</option>
		<option value="11">101~110 age</option>
</script>
