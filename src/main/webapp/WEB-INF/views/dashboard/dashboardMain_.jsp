 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
 <%@ include file="/WEB-INF/views/common/jstl.jsp"%>
 <%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	$(function(){
		var param = {}
		ajaxCallPostToken("/api/v1/user/info/seq", authToken, param, function(res){
			if(res.data){
				var obj = res.data;
				$(".iq").text(obj.iq);
				$(".test_cnt").text(obj.test_cnt);
				$(".ranking").text(obj.ranking);
				$(".iq-top").text("+"+obj.top+"%");
			}
		})
		
		
		ajaxCallPostToken("/api/v1/member/iq/new/test", authToken, param, function(res){
			var template = $.templates("#newList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".newList").html(htmlOutput);
		})
		
		$(document).on("click", ".btn-inprogress", function(){
			if(confirm("이어서 풀기를 진행하시겠습니까?")){
				var result_seq = $(this).attr("data-result-seq");
				var test_seq = $(this).attr("data-test-seq");
				location.href="/TestsDetail?seq="+test_seq+"&result_seq="+result_seq;
			}
			
		})
	})
</script>
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- Dashboard_section : s -->
		<section class="section section_dashboard">
			<div class="row row-con">

				<div class="col-lg-12 m_tit"><h2>DASHBOARD<b>Main</b></h2><br></div>

				<!-- Dashboard 1 : s -->
				<div class="col-md-4">
					<div class="dashboard-li">
						<h5>
							<span>My DeIQ</span>
						</h5>
						<p>
							<b class="iq"></b><a href="#" class="btn_add iq-top">+0.0005%</a>
						</p>

					</div>
				</div>
				<div class="col-md-4">
					<div class="dashboard-li">
						<h5>
							<span>Num. of tests</span>
						</h5>
						<p>
							<b class="test_cnt"></b> <span class="num-img"><img src="/resources/img/num-1.svg" alt="img" /></span>
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="dashboard-li">
						<h5>
							<span>IQ Ranking</span>
						</h5>
						<p>
							<b class="ranking"></b>th <span class="ranking-img"><img src="/resources/img/ranking-1.svg" alt="img" /></span>
						</p>
					</div>
				</div>
				<!-- Dashboard 1 : e -->

				<!-- Dashboard 2 : s -->
				<div class="col-md-7">
					<div class="dashboard-ui">
						<h2>
							<span>Normal Distribution</span>
						</h2>
						<div class="dashboard-gragh graph-loading"><img src="resources/img/dashboard_graph1.svg" alt="graph"></div><!--** 추가수정 20210722 **-->
					</div>
				</div>
				<div class="col-md-5">
					<div class="dashboard-ui">
						<h2>
							<span>In tellectual Growth Line</span>
						</h2>
						<div class="dashboard-gragh graph-loading"><img src="resources/img/dashboard_graph2.svg" alt="graph"></div><!--** 추가수정 20210722 **-->
					</div>
				</div>
				<!-- Dashboard 2 : e -->

				<!-- Dashboard 3 : s -->
				<div class="col-md-5">
					<div class="dashboard-ui">
						<h2>
							<span>Lastest Tests</span>
						</h2>
						<div class="dashboard-lasttest">
							<ul class="newList">
							
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="dashboard-ui">
						<h2>
							<span>Notice</span>
						</h2>
						<div class="dashboard-notice">
							<ul class="comingsoon-btn">
								<li><span class="img"><img src="/resources/img/notice-img.png" alt="img"></span>
									<h4>Bonus IQ Quiz uploaded!</h4> <a class="btn-close"></a></li>
								<li><span class="img"><img src="/resources/img/notice-img.png" alt="img"></span>
									<h4>
										Royboy acheived top IQ (170)<span>in Triangle number problem.</span>
									</h4> <a class="btn-close"></a></li>
								<li><span class="img"><img src="/resources/img/notice-img.png" alt="img"></span>
									<h4>
										YGenius follows you.<span>4 hours ago</span>
									</h4> <a class="btn-close"></a></li>
								<li><span class="img"><img src="/resources/img/notice-img.png" alt="img"></span>
									<h4>
										YGenius follows you.<span>4 hours ago</span>
									</h4> <a class="btn-close"></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Dashboard 3 : e -->
			</div>


		</section>
		<!-- Dashboard_section : e -->

	</div>
</div>

