 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
 <%@ include file="/WEB-INF/views/common/jstl.jsp"%>

<div id="container">
	<div class="wrapper">

		<!-- Dashboard_section : s -->
		<section class="section section_dashboard">
			<div class="row row-con">

				<!-- Dashboard 1 : s -->
				<div class="col-md-12">
					<div class="dashboard-ui">
						<div class="dashboard-view">
							<h1>
								Congratulations, <b>Sophia!</b>
							</h1>
							<p>
								You worked hard to earn your certificate from IQ Olympiad<br>share it with friends, and family to get the word out about how you are smart
							</p>
							<img src="/resources/img/Certification-img.png" alt="Certification" />
						</div>
					</div>
				</div>
				<!-- Dashboard 1 : e -->

				<!-- Dashboard 2 : s -->
				<div class="col-md-12">
					<div class="dashboard-ui">
						<h2>
							<span>My IQ Certificate’s uniqe link</span>
						</h2>
						<div class="row">
							<div class="col-md-12 dashboard-link">
								<a class="btn-copy">Copy & Share the link</a>
								<p>https://courses.edx.org/certificates/adfb25b21555423fba3448fcd1414908</p>
								<a class="ic-link"></a>
							</div>
						</div>
					</div>
				</div>
				<!-- Dashboard 2 : e -->
			</div>


		</section>
		<!-- Dashboard_section : e -->

	</div>
</div>
