 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
 <%@ include file="/WEB-INF/views/common/jstl.jsp"%>
 <%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	var page = 1;
	$(function(){
		var param = {}
		ajaxCallPostToken("/api/v1/user/info/seq", authToken, param, function(res){
			if(res.data){
				var obj = res.data;
				
				$(".iq-top").before(obj.name + " IQ is estimated to be in the ");
				$(".iq").text("IQ "+obj.iq);
				$(".ranking").text(obj.ranking + " th");
				$(".iq-top").text("top "+obj.top+"%");
				$(".iq-top-other").text("with "+obj.top_other);
			}
		})
		
		myList()
		
		
		param = {
		}
		ajaxCallPostToken("/api/v1/member/iq/category/test", authToken, param, function(res){
			var template = $.templates("#categoryList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".categoryList").html(htmlOutput);
		})
		
		$(".add-page-btn").click(function(){
			page++;
			myList();
		})
	})
	
	function myList(){
		param = {
				"page":page
			}
			ajaxCallPostToken("/api/v1/member/iq/test", authToken, param, function(res){
				
				if(res.paging.finalPageNo > page){
					$(".add-page-btn").show();
				}else{
					$(".add-page-btn").hide();
				}
				
				var template = $.templates("#myList"); // <!-- 템플릿 선언
		        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
		        if($(".myList").html().trim()){
			        $(".myList").find("tr:last-child").after(htmlOutput);
		        }else{
			        $(".myList").html(htmlOutput);
		        }
			})
	}
</script>
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- Dashboard_section : s -->
		<section class="section section_dashboard">
			<div class="row row-con">

				<!-- Dashboard 1 : s -->
				<div class="col-md-12">
					<div class="dashboard-ui">
						<h2 class="mt-none">
							<span>My IQ</span>
						</h2>
						<div class="dashboard-mypage">
							<div class="row">
								<div class="col-md-4">
									<h1>
										<span class="iq"></span><span>SD 15 <b class="ic-check"></b></span>
									</h1>
								</div>
								<div class="col-md-8">
									<ul>
										<li><span class="iq-top"></span> <span class="iq-top-other"></span> percentile
										</li>
										<li>IQ Olympiad Ranking: <span class="ranking"></span></li>
									</ul>
								</div>
								<div class="col-md-12 categoryList" >
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Dashboard 1 : e -->

				<!-- Dashboard 2 : s -->
				<div class="col-md-12">
					<div class="dashboard-ui" style="padding-bottom: 0;">
						<h2 style="margin-bottom: 0;">
							<span>Result Details</span>
						</h2>
						<div class="dashboard-data">
							<table cellpadding="0" cellspacing="0" border="0">
								<thead>
									<tr>
										<th>IQ Test Name</th>
										<th>IQ(SD 15)</th>
										<th>Raw</th>
										<th>Attempt</th>
										<th>Type</th>
										<th>Data</th>
										<th>Share</th>
									</tr>
								</thead>
								<tbody class="myList">
								</tbody>
							</table>
							<!--* 더보기 버튼 추가 : s *-->
							<div class="text-center add-page-btn" style="display: none">
								<a class="btn-add">더 보기</a>
							</div>
							<!--* 더보기 버튼 추가 : e *-->
						</div>
					</div>
				</div>
				<!-- Dashboard 2 : e -->
			</div>


		</section>
		<!-- Dashboard_section : e -->

	</div>
</div>
