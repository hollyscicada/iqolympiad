<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
	body{display: none}
</style>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	var global_test_seq = "${param.test_seq}";
	$(function() {
		
		
		var goMain = localStorage.getItem("goMain");
		if(goMain == 'Y'){
			localStorage.removeItem("goMain");
			$("body").show();
		}else{
			if(authToken){
				$(".btn-login-text").hide();
			}
			
			var custom_type = localStorage.getItem("custom_type");
			var custom_code = localStorage.getItem("custom_code");
			var is_test = localStorage.getItem("is_test");

			if (custom_type && custom_code) {
				var param = {
					"custom_type" : custom_type,
					"custom_code" : custom_code
				}
				ajaxCallPost("/api/v1/auto/login", param, function(res) {
					var custom_type = res.custom_type;
					var custom_code = res.custom_code;
					if (custom_type && custom_code) {
						localStorage.setItem("custom_type", custom_type);
						localStorage.setItem("custom_code", custom_code);
						
						if(global_test_seq && global_test_seq == '4'){
							location.href = "/TestsDetail?seq="+global_test_seq;		
						}else{
							location.href = "/dashboardMain";
						}
					} else {
						$("body").show();
					}
				})
			} else {
				
				
				/*이벤트*/
				if(global_test_seq && global_test_seq == '4'){
					location.href = "/TestsDetail?seq="+global_test_seq;	
					return;
				}
				
				
				if (is_test) {
					location.href = "/TestsMain";
				} else {
					$("body").show();
				}
			}
		}
		
		
		
		
		$(".start-iq-test-btn").click(function(){
			location.href='/TestsDetail?seq=1';
		})
		$(".btn-login").click(function(){
			location.href='/login';
		})
	})
</script>
<main role="main" class="main">
	<!-- container : s -->
	<div id="container">
		<div class="wrapper wrapper-full">

			<!-- Landing_section : s -->
			<section class="section section_landing">

				<!-- landing 1 : s -->
				<div class="landing_1st">
					<div class="landing-ui">
						<h2>
							Test your perceptive abilities<br>
							and achieve intellectual growth
						</h2>
						<div class="btn-box">
							<a class="btn-start start-iq-test-btn">Start IQ Test</a><span class="m_br"></span> <a class="btn-signin btn-login btn-login-text">I already have an account</a>
						</div>
						<img src="/resources/img/img-platform.svg" alt="img-platform" />
					</div>
				</div>
				<!-- landing 1 : e -->

				<!-- landing 2 : s -->
				<div class="landing_2nd">
					<div class="landing-ui row">
						<div class="col-md-6">
							<img src="/resources/img/img-service-01.svg" alt="img-service-01" />
						</div>
						<div class="col-md-6">
							<h3>
								The optimal way to test<br>
								your intelligence quotient
							</h3>
							<p>
								IQ Olympiad is an IQ testing platform that is designed to incentivize users to take, create, or curate IQ tests and build communities to socialize with other intellectuals
							</p>
						</div>
					</div>
				</div>
				<!-- landing 2 : e -->


				<!-- landing 3 : s -->
				<div class="landing_3nd">
					<div class="landing-ui row">
						<div class="col-md-6">
							<h3>IQ Olympiad is the ideal solution to evident problems of traditional IQ tests</h3>
							<ul>
								<li>Lack of Authority Verification</li>
								<li>Forgery & Manipulation</li>
								<li>Repetitive Tests</li>
								<li>Inaccessibility</li>
							</ul>
						</div>
						<div class="col-md-6">
							<img src="/resources/img/img-service-02.svg" alt="img-service-01" />
						</div>
					</div>
				</div>
				<!-- landing 2 : e -->

				<!-- landing 4 : s -->
				<div class="landing_2nd">
					<div class="landing-ui row">
						<div class="col-md-6">
							<img src="/resources/img/img-service-03.svg" alt="img-service-03" />
						</div>
						<div class="col-md-6">
							<h3>
								Blockchain technology<br>ensures transparency<br>and integrity of IQ certificates
							</h3>
							<p>
								IQ Olympiad leverages blockchain technology to achieve user incentivization, intellectual society formation, and tamper-proof IQ certificate issuance. IQO Token empowers the IQ Olympiad ecosystem by rewarding paricipants for their contribution and keeping them engaged in platform management through the IQ governance.
							</p>
						</div>
					</div>
				</div>
				<!-- landing 4 : e -->

				<!-- landing 5 : s -->
				<div class="landing_2nd">
					<div class="landing-ui row">
						<div class="col-md-12">
							<div class="bn-teststart">
								<h3>
									Test your intellectual abilities by taking the<br>Basic IQ Test!
								</h3>
								<a class="start-iq-test-btn">Start IQ Test</a>
							</div>
						</div>
					</div>
				</div>
				<!-- landing 5 : e -->




			</section>
			<!-- Landing_section : e -->

		</div>
	</div>
	<!-- container : e -->
</main>