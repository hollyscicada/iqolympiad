<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	$(function(){
		selectCountry();
		$(".submit-btn").click(function(){
			if(!authToken){
				if(confirm("Login is required to edit your profile. Would you like to sign in?")){
					location.href="/login"
				}
			}else{
				
				var	name = $("input[name=name]");
				var	age = $("select[name=age]");
				var	country_seq = $("select[name=country_seq]");
				var	img_seq = $("input[name=img_seq]");
				
				if(!name.val()){
					alert("Please enter your name.");
					name.focus();
					return;
				}
				if(!country_seq.val()){
					alert("Please select your country.");
					country_seq.focus();
					return;
				}
				if(!age.val()){
					alert("Please enter your date of birth.");
					age.focus();
					return;
				}
				if(confirm("Would you like to edit your profile?")){
					var param = {
						    "name":name.val(),
						    "age":age.val(),
						    "country_seq":country_seq.val(),
						    "img_seq":img_seq.val()
						}
					ajaxCallPostToken("/api/v1/change/member", authToken, param, function(res){
						if(res.success){
							alert("Your profile has been successfully edited.");
							location.href="/member/mypage"
						}
					})
				}
			}
			
		})
		
		
		
		$(document).on("click", ".profile-delete-btn", function(){
			if(confirm("Would you like to delete your profile image? You image cannot be recovered once deleted.")){
				$("input[name='img_seq']").val("0");
				$(".profile-img").attr("src", "/resources/img/user_img.png");
			}
		})
		$(document).on("click", ".img-change-btn", function(){
			$(".file_fun").trigger("click");
		})
		
		$(document).on("change", ".file_fun", function(){
			alert("Please stand by after closing this page..\n(File upload in progress)")
			ajaxUpload($(".file_fun")[0].files[0], "member", function(res) {
				$("input[name='img_seq']").val(res.file_seq);
				$(".profile-img").attr("src", res.url);
			})
		})
		
		
	})
	
	function selectCountry(){
		var param = {
				
		}
		ajaxCallPost("/api/v1/country", param, function(res){
			var template = $.templates("#countryList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".country-select").html(htmlOutput);
	        
	        var param = {}
			ajaxCallPostToken("/api/v1/user/info/seq", authToken, param, function(res){
				if(res.data){
					var obj = res.data;
					$("input[name=name]").val(obj.name);
					$("select[name=age]").val(obj.age);
					$("select[name=country_seq]").val(obj.country_seq);
					$("input[name=img_seq]").val(obj.img_seq);
					
					if(obj.img_url){
						$(".profile-img").attr("src", obj.img_url);
					}
					
				}
			})
	        
	        
		})
	}
</script>
<input name="img_seq" type="hidden" value="">
<input type="file" class="file_fun" style="display: none">
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- My Account_section : s -->
		<section class="section section_myAccount">

			<!-- myAccount : s -->
			<div class="row row-con">
				<div class="col-lg-12 m_tit"><h2>ACCOUNT SETTING</h2><br></div>
				<div class="col-lg-12">
					<div class="account-ui">
						<h2>
							<span>My Account</span>
						</h2>
						<!-- profile : s -->
						<div class="account-li row">
							<div class="col-md-5">
								<div class="profile-img">
									<span class="img-change-btn"> 
										<img class="profile-img" src="${sessionScope.MEMBER.img_url }" onerror="this.src='/resources/img/user_img.png'" alt="img" /> 
										<a class="ic-edit"></a>
									</span> 
									<a class="btn-edit profile-delete-btn">Delete</a>
								</div>
							</div>
							<div class="col-md-7">
								<ul class="profile-edit">
									<li>
										<span>Username</span>
										<input type="text" name="name" placeholder="iqolympiad" />
									</li>
									<li>
										<span>Country</span>
										<select name="country_seq" class="country-select"></select>
									</li>
									<li>
										<span>Age</span>
										<c:set var="nowNum" value="2021"></c:set>
										<select name="age">
											<c:forEach begin="1900" end="${nowNum}" step="1" var="i">
												<option value="${nowNum-i+1900}">${nowNum-i+1900}</option>
											</c:forEach>
										</select>
									</li>
								</ul>
								<a class="btn-save submit-btn">Save</a>
							</div>
						</div>
						<!-- profile : e -->
					</div>
				</div>
			</div>
			<!-- myAccount : e -->
		</section>
		<!-- My Account_section : e -->

	</div>
</div>
