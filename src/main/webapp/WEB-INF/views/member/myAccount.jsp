<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	$(function(){
		var param = {}
		ajaxCallPostToken("/api/v1/user/info/seq", authToken, param, function(res){
			if(res.data){
				var obj = res.data;
				$(".mypage-name").text(obj.name);
				$(".mypage-country-code").text(obj.name_code);
				$(".mypage-age").text(obj.age);
				
				if(obj.img_url){
					$(".mypage-img-url").attr("src", obj.img_url);
				}
				
				var sex = obj.sex;
				var sex_name = ""
				if(obj.sex == 'M'){
					sex_name = "Male"
				}
				if(obj.sex == 'W'){
					sex_name = "Female"
				}
				$(".mypage-sex").text(sex_name);
				
			}
		})
		$(".mypage-update-btn").click(function(){
			if(!authToken){
				if(confirm("Login is required to edit your profile. Would you like to sign in?")){
					location.href="/login"
				}
			}else{
				location.href="/member/mypage/edit"
			}
			
		})
	})
</script>
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- My Account_section : s -->
		<section class="section section_myAccount">

			<!-- myAccount : s -->
			<div class="row row-con">
				<div class="col-lg-12 m_tit"><h2>ACCOUNT SETTING</h2><br></div>
				<div class="col-lg-12">
					<div class="account-ui">
						<h2>
							<span>My Account</span>
						</h2>
						<!-- profile : s -->
						<div class="account-li row">
							<div class="col-md-5">
								<div class="profile-img">
									<span><img  class="mypage-img-url" src="${sessionScope.MEMBER.img_url }" onerror="this.src='/resources/img/user_img.png'" alt="img" /></span> 
									<a class="btn-edit mypage-update-btn">Edit</a>
								</div>
							</div>
							<div class="col-md-7">
								<ul class="profile-li">
									<li>Name<span class="mypage-name"></span></li>
									<li>Country<span class="mypage-country-code"></span></li>
									<li>Gender<span class="mypage-sex"></span></li>
									<li>Year of birth<span class="mypage-age"></span></li>
									<li>Show my IQ results
										<div class="onoff">
											<input type="checkbox" name="onoff" class="onoff-checkbox" id="myonoff" /> 
											<label class="onoff-label" for="myonoff"> <span class="onoff-inner">
												</span> <span class="onoff-switch"></span>
											</label>
										</div>
									</li>
								</ul>
								<p>You have to show your results to be listed in the ranking.</p>
							</div>
						</div>
						<!-- profile : e -->
					</div>
				</div>
			</div>
			<!-- myAccount : e -->
		</section>
		<!-- My Account_section : e -->

	</div>
</div>
