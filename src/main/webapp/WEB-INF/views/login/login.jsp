<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
  <%
  java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("config-"+System.getProperty("spring.profiles.active"));
  String domain_url = resource.getObject("domain.url").toString();
  String facebook_key = resource.getObject("facebook.key").toString();
  String facebook_url = resource.getObject("facebook.redirect.url").toString();
  String google_key = resource.getObject("google.key").toString();
  String google_url = resource.getObject("google.redirect.url").toString();
	%>
<script>

	var domain_url = "<%=domain_url%>";
	var facebook_key = "<%=facebook_key%>";
	var facebook_url = "<%=facebook_url%>";
	var google_key = "<%=google_key%>";
	var google_url = "<%=google_url%>";
</script>

<script>
	var _newUA = navigator.userAgent;
	if(_newUA.indexOf("KAKAOTALK")>-1){
		_newUA = _newUA.substring(0, _newUA.indexOf("KAKAOTALK"));
	}
	Object.defineProperty(window.navigator, "userAgent",{
		get:function(){return _newUA}
	})
	function google(){
		location.href=`
			https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/userinfo.email&
			access_type=offline&
			include_granted_scopes=true&state=state_parameter_passthrough_value&
			redirect_uri=`+google_url+`&response_type=code&client_id=`+google_key;
	}
	function facebook(){
		location.href="https://www.facebook.com/v9.0/dialog/oauth?client_id="+facebook_key+"&redirect_uri="+facebook_url+"&state=a&resource_type=token";
	}
</script>
<style>
@media (max-width:1199px) {
#footer{position:absolute; bottom:0; left:0;}
}
</style>
<!-- container : s -->
<div id="container">
	<div class="wrapper signinBg">
		<!-- sign In : s -->
		<div class="signInup">
			<div class="signInup-inner">
				<h1>Sign <c:if test="${empty param.type}">in</c:if><c:if test="${param.type eq 'sign' }">up</c:if></h1>
				<div class="btn-box">
					<a href="javascript:google()" class="btn-google"></a> 
<!-- 					<a href="javascript:facebook" class="btn-facebook"></a> -->
				</div>
			</div>
		</div>
		<!-- sign Iin : e -->
	</div>
</div>
<!-- container : e -->