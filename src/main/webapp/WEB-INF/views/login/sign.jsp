<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script>
	var login_type = "${param.login_type}";
	var login_code = "${param.login_code}";
	
	
	$(function(){
		selectCountry();
		
		
		$(".sign-up-btn").click(function(){
			var	name = $("input[name=name]");
			var	age = $("select[name=age]");
			var	sex = $("select[name=sex]");
			var	country_seq = $("select[name=country_seq]");
			var	email = $("input[name=email]");
			
			if(!name.val()){
				alert("Please enter your name.");
				name.focus();
				return;
			}
			if(!age.val()){
				alert("Please enter your date of birth.");
				age.focus();
				return;
			}
			if(!sex.val()){
				alert("Please select your gender.");
				sex.focus();
				return;
			}
			if(!country_seq.val()){
				alert("Please select your country.");
				country_seq.focus();
				return;
			}
			if(!email.val()){
				alert("Please enter your email.");
				email.focus();
				return;
			}
			
			if(email.val().indexOf("@") == -1){
				alert("email 형식이 올바르지 않습니다.");
				email.focus();
				return;
			}
			
			if(email.val().indexOf("@") == 0 || (email.val().indexOf("@")+1) == email.val().length){
				alert("You have entered an invalid email address.");
				email.focus();
				return;
			}
			if(confirm("Would you like to sign up?")){
				var param = {
				    "login_type":login_type,
				    "login_code":login_code,
				    "email":email.val(),
				    "name":name.val(),
				    "sex":sex.val(),
				    "age":age.val(),
				    "country_seq":country_seq.val()
				}
				ajaxCallPost("/api/v1/sign", param, function(res){
					if(res.success){
						alert("Your registration has been completed.");
						location.href="/sign/success";
					}
				})
			}
			
		})
	})
	
	function selectCountry(){
		var param = {
				
		}
		ajaxCallPost("/api/v1/country", param, function(res){
			var template = $.templates("#countryList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".country-select").html(htmlOutput);
		})
	}
</script>
<!-- container : s -->
<div id="container">
	<div class="wrapper signformBg">

		<!-- sign Up : s -->
		<div class="signInup signInup_form">
			<div class="signInup-inner">
				<h1>Sign up</h1>
				<div class="signup_form">
					<ul>
						<li>
							<span>Username</span>
							<input type="text" name="name" placeholder="iqolympiad" />
						</li>
						<li>
							<span>Year of Birth</span>
							<c:set var="nowNum" value="2021"></c:set>
							<select name="age">
								<c:forEach begin="1900" end="${nowNum}" step="1" var="i">
									<option value="${nowNum-i+1900}">${nowNum-i+1900}</option>
								</c:forEach>
							</select>
						</li>
						<li>
							<span>Sex</span>
							<select name="sex">
								<option value="">Select</option>
								<option value="M">Male</option>
								<option value="W">Female</option>
							</select>
						</li>
						<li>
							<span>Country</span>
							<select name="country_seq" class="country-select"></select>
						</li>
						<li>
							<span>Email</span>
							<input type="text" name="email" placeholder="iqolympiad@gmail.com" />
						</li>

					</ul>
				</div>
				<div class="btn-box">
					<a class="btn-blue sign-up-btn">Sign up</a>
				</div>
			</div>
		</div>
		<!-- sign Up : e -->

	</div>
</div>
<!-- container : e -->