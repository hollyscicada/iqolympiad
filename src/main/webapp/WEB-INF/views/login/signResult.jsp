<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script>

	$(function(){
		
		var result_seq = sessionStorage.getItem("result_seq");
		var authToken = "${sessionScope.MEMBER.authToken}";
		
		if(result_seq && authToken){
			var param = {
					"result_seq":result_seq
			}
			ajaxCallPostToken("/api/v1/iq/test/save/update", authToken, param, function(res){
				if(res.success){
					alert("Your IQ Test result has been saved.");
					sessionStorage.removeItem("result_seq");
					initSuccess(res);
				}else{
					initFail();
				}
			})
		}
		
		function init(){
			var custom_type = "${sessionScope.MEMBER.custom_type}"
			var custom_code = "${sessionScope.MEMBER.custom_code}"
			localStorage.setItem("custom_type", custom_type);
			localStorage.setItem("custom_code", custom_code);
		}
		
		function initSuccess(res){
			init();
			if(res.redirectType == 'RESULT'){
				location.href="/TestsResult?result_seq="+res.data.result_seq;
			}else{
				if(res.data && res.data.event_yn && res.data.event_yn == 'Y'){
					location.href="/TestsResultEvent?result_seq="+res.data.result_seq;
				}
			}
			
		}
		function initFail(){
			init();
		}
		
		
		$(".basic-test-start").click(function(){
			location.href="/TestsDetail?seq=1";
		})
	})
</script>
<!-- container : s -->
<div id="container">
	<div class="wrapper signupBg">
		<!-- sign Up_welcom : s -->
		<div class="signInup">
			<div class="signInup-inner">
				<h1>
					Welcome!<br>
					<img src="/resources/img/h_logo.png" alt="logo">
				</h1>
				<div class="btn-box">
					<a class="btn-blue basic-test-start">Start IQ Test</a>
				</div>
			</div>
		</div>
		<!-- sign Up_welcom : e -->

	</div>
</div>
<!-- container : e -->