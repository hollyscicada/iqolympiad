<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script src="/resources/js/clipboard.min.js"></script>
	<style>
			.wrapper{padding:100px 0 0;}
			@media (max-width:1199px) {
				.wrapper.wrapper-70{padding: 10px 0 0;}
			}
		</style>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	var global_result_seq = "${param.result_seq}";
	$(function(){
		new ClipboardJS('.copy-btn');
		$("#url").val(location.href);
		
		var param = {
				"result_seq":global_result_seq
		}
		ajaxCallPost("/api/v1/iq/test/result/event", param, function(res){
			if(res.data){
				var obj = res.data;
				var score = obj.score;
				$(".member-name").html(obj.member_name + ",<span>Your IQ test result is</span>");
				$(".result-iq").text("IQ "+score);
				
				
				var event_img_seq = obj.event_img_seq;
				
				if(event_img_seq){
					$(".similar-img").attr("src", obj.event_img_url)
					$(".similar").text(obj.event_name)
					$(".iq-top").text("top "+obj.event_percent+"th");
					
					
					
					param = {
							"score":score
					}
					ajaxCallPostToken("/api/v1/iq/event/result/resources", authToken, param, function(res){
						
						if(res.data && res.data.bestMap && res.data.jobList ){
							
							var template = $.templates("#jobList"); // <!-- 템플릿 선언
					        var htmlOutput = template.render(res.data); // <!-- 렌더링 진행 -->
					        $(".jobList").html(htmlOutput);
						}
						
					})
					
					
					
					
					
					
				}else{
					param = {
							"score":score
					}
					ajaxCallPostToken("/api/v1/iq/event/result/resources", authToken, param, function(res){
						
						if(res.data && res.data.bestMap && res.data.jobList ){
							$(".similar-img").attr("src", res.data.bestMap.img_url)
							$(".similar").text(res.data.bestMap.name)
							$(".iq-top").text("top "+res.data.bestMap.percent+"th");
							
							var template = $.templates("#jobList"); // <!-- 템플릿 선언
					        var htmlOutput = template.render(res.data); // <!-- 렌더링 진행 -->
					        $(".jobList").html(htmlOutput);
					        
					        
					        
					        param = {
					        		"event_seq":res.data.bestMap.event_seq,
					        		"result_seq":global_result_seq
					        }
					        ajaxCallPostToken("/api/v1/iq/event/result/resources/upd", authToken, param, function(res){
					        	
					        })
					        
						}
						
					})
				}
				
			}
		})
		
		$(".copy-btn").click(function(){
			alert("The link has been copied.")
		})
		
	})
</script>
<input type="text" id="url" value="">
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- eventResult_section : s -->
		<section class="section section_event">
			<div class="row row-con">
				<!-- Event Result : s -->
				<div class="event-ui">
					<!-- Event Result 01 : s -->
					<div class="event-result event-result-01 row">
						<div class="col-lg-6 col-md-12 col-left">
							<h3 class="member-name">
							</h3>
						</div>
						<div class="col-lg-6 col-md-12 col-right">
							<h1>
								<b class="result-iq"></b><span>You are in the <b class="iq-top"></b> percentile
								</span>
							</h1>

						</div>
					</div>
					<!-- Event Result 01 : e -->

					<!-- Event Result 02 : s -->
					<div class="event-result event-result-02">
						<h2>
							<span>Which celebrity has a similar IQ ?</span>
							<span class="similar" style="font-weight: bold;"></span>
						</h2>
						<img src="" class="similar-img" alt="img" />
					</div>
					<!-- Event Result 02 : e -->

					<!-- Event Result 03 : s -->
					<div class="event-result event-result-03">
						<p>You might perform well as a(an)</p>
						<ul class="jobList">
						</ul>
					</div>
					<!-- Event Result 03 : e -->

					<!-- Event Result 04 : s -->
					<div class="event-result event-result-04">
						<ul>
							<li><a class="ic-share copy-btn" data-clipboard-target="#url">Share my IQ</a></li>
							<li><a href="/TestsDetail?seq=1" class="ic-arrow">Official IQ Test</a></li>
						</ul>
						<p>
							Take the Basic IQ Test to get your own certificate from an elaborate IQ test at <a href="/">IQ OLYMPIAD</a>
						</p>
					</div>
					<!-- Event Result 04 : e -->

				</div>
				<!-- Event Result : e -->
			</div>


		</section>
		<!-- eventResult_section : e -->

	</div>
</div>
