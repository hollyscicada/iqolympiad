<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script src="/resources/js/swiper.min.js"></script>
<style>
	.current-complate{
		background: red !important;
		color: #fff !important
	}
	@media (max-width:1199px) {
 		.swiper-wrapper{
 			width: 80%;
		}
 	}
</style>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	var global_seq = "${param.seq}";
	var global_ing_time = 0;
	var tid; //타이머
	var global_question_cnt = 0;
	var view_question = 0; //이어풀기, 신규풀기시 문항 dom의 순번, 문항중 낮은번호의 문항이 맨처음에 노출된다.
	var global_question_list = [];
	var type = "";
	var global_result_seq = "${param.result_seq}";
	var write_type;
	var globalEventYn = "";

	var is_first = true;


	$(function(){


		var _newUA = navigator.userAgent;
		if(_newUA.indexOf("KAKAOTALK")>-1){
			alert("Kakao Browser needs to take an IQ test.");
		}
		Object.defineProperty(window.navigator, "userAgent",{
			get:function(){return _newUA}
		})


		/*테스트 상세정보 가져오기*/
		selectTestDetail();

		/*테스트 시작 버튼*/
		$(".btnStart").click(function(e){
			if(global_result_seq){
				chkTestStart();
				return;
			}
			if(write_type == 'M' && !authToken){
				if(confirm("Login is required to take UCT IQ Tests.\nWould you like to sign in?")){
					location.href="/login";
				}
				return;
			}
			if(write_type == 'M'){
				if(confirm("'50' points will be deducted to take a UCT IQ Test.\nWould you like to proceed?\nThis test allows 2 attempts.")){
					chkTestStart();
					return;
				}
			}else{
				var msg = "Would you like to take the IQ Test?";

				if(authToken && globalEventYn == 'N'){
					msg += '\nYou may take up to 2 tests.';
				}
				if(confirm(msg)){
					chkTestStart();
					return;
				}
			}
		})

		$(".arrow-prev").click(function(){
			if(view_question > 0){
				nextQuestionView("N");
			}
		})

		$(".arrow-next").click(function(){
			if(global_question_cnt > (view_question+1)){
				nextQuestionView("Y");
			}
		})
		/*제출하기 버튼1*/
		$(".iq-test-submit").click(function(){
			$(".testComplete-ui-submit").show();
			$(".test-ui").hide();
		})
		/*서브밋버튼*/
		/*
		$(".btn-submit-add-temp").click(function(){
			var msg = "현재진행상황 임시저장하시겠습니까?\n저장하려면 로그인이 필요합니다.\n로그인페이지로 이동하시겠습니까?";
			if(authToken){
				msg = "현재진행상황을 임시저장하시겠습니까?";
			}
			if(confirm(msg)){
				type = "TEMPSAVE";
				save();
			}

		})
		*/
		$(".btn-submit-add").click(function(){
			/*
			if(!authToken){
				alert("As a guest, you may only temporarily save your progress.");
				return;
			}
			*/
			if(confirm("Would you like to save your results?")){
				type = "END";
				save();
			}
		})
		/*취소버튼*/
		$(".btn-cancel").click(function(){
			$(".testComplete-ui-submit").hide();
			$(".test-ui").show();
		})
		/*팝업 닫기*/
		$(".popClose").click(function(){
			if($(".test-ui").css("display") == 'none'){
				//서브밋 버튼 팝업
				$(".testComplete-ui-cancel").hide();
				$(".testComplete-ui-submit").hide();
				$(".test-ui").show();
			}else{

				if(globalEventYn == 'Y'){
					$(".test-ui").hide();
					$(".pop").hide();
					$(".popBg").hide();
					return;
				}

				var msg = "Would you like to temporarily save your progress?\nLogin is required to save your progress.\nWould you like to sign in?";
				if(authToken){
					msg = "Would you like to temporarily save your progress?";
				}
				if(confirm(msg)){
					type = "TEMPSAVE";
					save();
				}else{
					//퀴즈 팝업
					$(".test-ui").hide();
					$(".pop").hide();
					$(".popBg").hide();
				}

			}
		})

		/*
			하단에 추천 테스트 클릭시
		*/
		$(document).on("click", ".iq-test-item", function(res){
			var seq = $(this).attr("data-seq");
			location.href="/TestsDetail?seq="+seq;
		})

		/**
		보기 선택시
		*/
		$(document).on("click", ".example-chk", function(res){

			var currentChk = $(this).parents(".input-li").find("input").is(":checked");
			var input = $(this).parents(".input-li").find("input");
			setTimeout(function(){

				if(currentChk){
					input.attr("checked", false);
					nextQuestionView("F");
					return;
				}


				nextQuestionView("Y");
			}, 100)
		})
		$(document).on("click", ".footer-question-sta", function(res){
			var index = $(this).attr("data-que-index");
			view_question = parseInt(index);


			questionHide();
			$(".question-dom-"+view_question).show();
			footerQuestionHide();

			syncChkQuestionNext();
		})



		if(global_result_seq){
			$(".btnStart").trigger("click");
		}

	})

	function goReload(){
		location.href='/TestsDetail?seq='+global_seq
	}

	function chkTestStart(){
		if(authToken){

			if(global_result_seq){
				startTest();
			}else{
				/*회원은 2회*/
				if(globalEventYn == 'Y'){
					startTest();
					return;
				}
				var param = {}
				ajaxCallPostToken("/api/v1/iq/test/"+global_seq+"/cnt", authToken, param, function(res){
					if(res.success){
						startTest();
					}else{
						alert("This test allows 2 attempts.");
						return;
					}
				})
			}

		}else{

			/*비회원은 제한없음*/
			startTest();
		}
	}
	/*제출 및 임시저장*/
	function save(){

		var example_seqs = [];
		var testa_inputs = $(".testa_input");
		for(var i = 0 ; i < testa_inputs.length ; i++){
			if(testa_inputs.eq(i).is(':checked')){
				var inputLi = testa_inputs.eq(i).parents(".input-li");
				var question_seq = inputLi.attr("data-question-seq");
				var example_seq = inputLi.attr("data-example-seq");

				var example_seq = {
						"question_seq":question_seq,
						"example_seq":example_seq
				};
				example_seqs.push(example_seq)
			}
		}

		var param = {
			    "result_seq":global_result_seq,
			    "type":type,
			    "example_seqs":example_seqs,
				"event_yn":globalEventYn
			}

		if(!example_seqs || !example_seqs.length){
			alert("You must answer at least 1 question.")
			return;
		}
		ajaxCallPostToken("/api/v1/iq/test/end", authToken, param, function(res){
			if(res.success){

				if(authToken){
					if(type == 'END'){
						alert("You have completed the IQ Test.");

						if(globalEventYn == 'N'){
							location.href="/TestsResult?result_seq="+global_result_seq;
						}else if(globalEventYn == 'Y'){
							location.href="/TestsResultEvent?result_seq="+global_result_seq;
						}
					}else{
						alert("You have temporarily saved your progress.")
						location.href="/dashboardMain"
					}
				}else{
					sessionStorage.setItem("result_seq", global_result_seq);
					location.href="/login";
				}
			}
		})
	}
	/**
		시간초 타이머
	*/
	function timer() {	// 1초씩 카운트
		var minit = Math.floor(global_ing_time/60);
		if(minit < 10) minit = "0"+minit;

		var second = global_ing_time%60;
		if(second < 10) second = "0"+second;

		var time = minit + ":" + second;
		$(".ing_time").text(time);

		global_ing_time--;					// 1초씩 감소
		if (global_ing_time < 0) {			// 시간이 종료 되었으면..
			clearInterval(tid);		// 타이머 해제


			alert("Your time is up.\nThe test will be closed.");
			if(authToken){
				location.href="/dashboardMain";
			}else{
				location.href="/";
			}
		}
	}


	function makeResultSeq(){
		var param = {
				"test_seq":global_seq,
				"event_yn":globalEventYn
			}
		ajaxCallPostToken("/api/v1/iq/result/test/add", authToken, param, function(res){
			if(res.success){
				global_result_seq = res.data.result_seq;
				realStart();
			}
		})
	}
	/*
		테스트 시작함수
	*/
	function startTest(){

		if(!is_first){
			$(".test-ui").show();
			$(".pop").show();
			$(".popBg").show();
			return;
		}
		is_first = false;


		if(!global_result_seq){
			global_result_seq = makeResultSeq();
		}else{
			realStart();
		}
	}

	function realStart(){
		if(global_result_seq){
			var param = {
					"result_seq":global_result_seq,
					"event_yn":globalEventYn
			}
			ajaxCallPostToken("/api/v1/iq/test/ing", authToken, param, function(res){

				if(res.data){
					var obj = res.data;
					global_question_list = obj.question_list;

					//총문항수
					$(".question-cnt").html(global_question_cnt);


					//문항 셋팅
					if(global_question_list){
						var template = $.templates("#question_list"); // <!-- 템플릿 선언
				        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
				        $(".question_list").html(htmlOutput);
					}

					//최초1번 //프로그레스바 시작번호 구하기 위해서
					var solve_cnt = 0;
					if(global_question_list){
						for(var i = 0 ; i < global_question_list.length ; i++){
							if(global_question_list[i].solve_yn == 'Y'){
								solve_cnt++;
							}
						}
					}
					//프로그래스바 최소값 최초1번
					$(".progress-bar").attr("aria-valuemin", solve_cnt);

					//프로그래스바 최대값 최초1번
					$(".progress-bar").attr("aria-valuemax", global_question_cnt);

					//화면에 보이는 문항 순번 체크
					/*
					if(global_question_list){
						view_question = 0;
						var is_view_question = true;
						for(var i = 0 ; i < global_question_list.length ; i++){
							if(global_question_list[i].solve_yn != 'Y'){
								if(is_view_question) {
									view_question = i;
									is_view_question = false;
								}
							}
						}
					}
					*/
					view_question = 0;




					/*계속 갱신 */
					syncChkQuestionNext();



					//하단 문항 현황
					if(global_question_list){
						var template = $.templates("#questionStatus"); // <!-- 템플릿 선언
				        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
				        $(".question-status").html(htmlOutput);
					}



					//화면에 노출할 문항 dom 번호
					questionHide();
					$(".question-dom-"+view_question).show();
					footerQuestionHide();

					$('html').scrollTop(0);

				}

				//타이머 시작
				global_ing_time = obj.use_time ? obj.use_time : 0;
				clearInterval(tid);
				tid=setInterval(timer,1000);


				$(".testComplete-ui").hide();
				$(".testComplete-ui").hide();
				$(".test-ui").show();
				$(".pop").show();
				$(".popBg").show();
			})
		}
	}

	/*문항 넘길때마다1*/
	function nextQuestionView(ins){
		if(ins == 'Y'){
			view_question++;
		}else if(ins == 'N'){
			view_question--;
		}


		if(global_question_cnt == view_question){
			syncChkQuestionNext();
		}
		if(global_question_cnt > view_question){
			//화면에 노출할 문항 dom 번호
			questionHide();
			$(".question-dom-"+view_question).show();
			footerQuestionHide();
			syncChkQuestionNext();
		}


		/*하단의 이전, 다음버튼 셋팅*/
		if(view_question == 0){
			$(".arrow-prev").addClass("off");
		}else{
			$(".arrow-prev").removeClass("off");
		}

		if(global_question_cnt > (view_question+1)){
			$(".arrow-next").removeClass("off");
		}else{
			$(".arrow-next").addClass("off");
		}



		/*하단바 셋팅*/
		var testAs = $(".testA");

		for(var i = 0 ; i < testAs.length ; i++){
			var is_solve = false;
			var inputs = testAs.eq(i).find(".testa_input");
			for(var x = 0 ; x < inputs.length ; x++){
				if(inputs.eq(x).is(':checked')){
					is_solve = true;
				}
			}
			if(is_solve){
				$(".footer-question-sta").eq(i).addClass("complete")
			}
		}
	}

	/*문항 넘길때마다2*/
	function syncChkQuestionNext(){
		/*문제 몇개풀엇는지 */
		var solve_cnt = 0;//현재 몇문항을 풀었는지, 몇번까지가 아닌 몇문항이다.
		var solve_cnt_text = 1;//현재 몇문항을 풀었는지, 몇번까지가 아닌 몇문항이다.

		var testa = $(".testa_input");//현재 몇문항을 풀었는지, 몇번까지가 아닌 몇문항이다.
		for(var i = 0 ; i < testa.length ; i++){
			if(testa.eq(i).is(':checked')){
				solve_cnt++;
				solve_cnt_text++;
			}
		}

		//현재 몇문항을 풀었는지
		$(".solve_cnt").html(solve_cnt_text < global_question_cnt ? solve_cnt_text : global_question_cnt);

		//프로그레스바 가로 채움 css
		var width = (solve_cnt/global_question_cnt)*100;
		$(".progress-bar").css("width", width + '%');
	}

	/*문항 전체 가리기*/
	function questionHide(){
		$(".testQ").hide();
		$(".testA").hide();
	}
	/*하단 문항셋팅*/
	function footerQuestionHide(){
		$(".footer-question-sta").removeClass("current-complate");
		$(".footer-question-sta").eq(view_question).addClass("current-complate");



		$(".mobile-current-question").text(view_question+1);
	}


	/*
		테스트 상세정보 매핑
	*/
	function selectTestDetail(){
		var param = {

		}
		ajaxCallPost("/api/v1/iq/test/"+global_seq, param, function(res){
			if(res.data){
				var obj = res.data;

				if(obj.write_type == 'A'){
					$(".detail-write-iq").hide();
				}


				globalEventYn = obj.event_yn;

				write_type = res.data.write_type;
				$(".detail-title").text(obj.title);
				$(".detail-write-name").text(obj.write_name);

				var write_iq = obj.write_iq ? obj.write_iq : "0";
				$(".detail-write-iq").html("<b>IQ "+write_iq+"</b>");
				$(".detail-test-create-cnt").html(obj.test_create_cnt+" tests");

				var write_img_url = obj.write_img_url ? obj.write_img_url : "";
				$(".detail-write-img-url").attr("src", write_img_url)

				$(".detail-category-title").text(obj.category_title);
				$(".detail-question-cnt").html(obj.question_cnt + " Questions");
				global_question_cnt = obj.question_cnt;

				$(".ic-untimes").text(obj.ing_time);

				$(".subTop-img img").attr("src", obj.detail_img_url);
				$(".subTop-img").show();


				$(".detail-content").html(obj.content);
				$(".detail-sub-content").html(obj.title + "<br>" + "Last Update: " + obj.test_update_dt + ", n="+obj.one+" (1st attempt), n="+obj.two+" (2nd attempt)");



				if(obj.write_type == 'A'){

					if(globalEventYn == 'Y'){
						var template = $.templates("#rankingTbodyEvent"); // <!-- 템플릿 선언
				        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
				        $(".ranking-tbody").html(htmlOutput);
					}else if(globalEventYn == 'N'){
						var template = $.templates("#rankingTbody"); // <!-- 템플릿 선언
				        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
				        $(".ranking-tbody").html(htmlOutput);
					}
				}else if(obj.write_type == 'M'){
					if(obj.ranking_list){
						var template = $.templates("#rankingTbodyMember"); // <!-- 템플릿 선언
				        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
				        $(".ranking-tbody").html(htmlOutput);
					}
				}




				if(obj.recommend_list){
					var template = $.templates("#recommendList"); // <!-- 템플릿 선언
			        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
			        $(".detail-recommend-list").html(htmlOutput);
				}


				if(obj.history_list){
					var template = $.templates("#historyList"); // <!-- 템플릿 선언
			        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
			        $(".detail-history-list").html(htmlOutput);
				}



				if(obj.best_ranking_list && obj.best_ranking_list.length){
					var template = $.templates("#bestRankingList"); // <!-- 템플릿 선언
			        var htmlOutput = template.render(obj); // <!-- 렌더링 진행 -->
			        $(".detail-best-ranking-list").html(htmlOutput);
				}else{
					$(".detail-best-div").hide();
				}

				if(globalEventYn == 'Y'){
					$(".detail-best-div").hide();
				}




				var swiper = new Swiper('.recommend-top-div', {
		            slidesPerView: '3.2',
		            touchRatio: 1,
		            breakpoints : {
		                1199 : {
		                    slidesPerView : 'auto',
		                    touchRatio: 1,
		                },
		            },
		        });




			}
		})

	}

$(document).ready(function(){

    var scroll_pos = 0;

    $(document).scroll(function() {

        scroll_pos = $(this).scrollTop();
        if(scroll_pos > 300) {
        	if($(".quickBox").css('top') == '300px'){
	        	$(".quickBox").animate({
	        			top: "30px",
	        		},
	        		500);
	        	setTimeout(function(){
		        	$(".quickBox").css("position", "fixed");
        		} ,500)
        	}
        } else {

        	if($(".quickBox").css('top') == '30px'){
        		$(".quickBox").animate({
        			top: "300px",
        		},
        		500);
        		setTimeout(function(){
	        		$(".quickBox").css("position", "absolute");
        		} ,500)
        	}

        }
    });

});


</script>
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- TestDetail_section : s -->
		<section class="section section_testsDetail">

			<!-- subtopImg : s -->
			<div class="subTop">
				<div class="subTop-img" style="display: none">
					<img alt="img" />
				</div>
			</div>
			<!-- subtopImg : e -->

			<!-- QuickBox : s -->
			<div class="quickBox">
				<!-- Quick1_btnBox : s -->
				<div class="btnBox">
					<a href="#" class="ic-share"></a><a class="btnStart">Start this test</a>
					<div class="btnBox-notice">
						<span class="notice-top detail-title"></span> <span class="notice-bottom">You could see some samples of this test before paying your points.</span>
					</div>
				</div>
				<!-- Quick1_btnBox : e -->

				<!-- Quick2_ infoBox : s -->
				<div class="infoBox">
					<div class="row">
						<div class="col-lg-12 col-xs-5">
							<span class="ic-star">0.0</span>
						</div>
						<div class="col-lg-12 col-xs-7"><!--**20210722 수정**-->
							<span class="ic-untime ic-untimes"></span>
							<span class="ic-type detail-category-title"></span>
							<span class="ic-list detail-question-cnt"></span>
						</div>
					</div>
				</div>
				<!-- Quick2_infoBox : e -->
			</div>
			<!-- QuickBox : e -->


			<!-- Detail: s -->
			<div class="row row-con">
				<div class="col-lg-12">
					<div class="detail-ui">
						<!-- profile : s -->
						<h1 class="detail-title"></h1>
						<ul class="profile">
							<li class="profile-img"><span><img class="detail-write-img-url" onerror="this.src='/resources/img/user_img.png'" alt="img" /></span></li>
							<li class="profile-txt"><h4>
									<span class="detail-write-name"></span><br>
									<span class="detail-write-iq"><b></b></span>
									<span>Created <b class="detail-test-create-cnt"></b></span>
								</h4></li>
						</ul>
						<!-- profile : e -->

						<!-- explain : s -->
						<div class="detail-li">
							<h2>About this test</h2>
							<p class="detail-content">
							</p>
						</div>
						<div class="detail-li">
							<h2>Statistics of this test</h2>
							<p  class="detail-sub-content">
							</p>
						</div>
						<!-- explain : e -->

						<!-- gragh : s -->
						<div class="detail-gragh graphfull-loading"><img src="resources/img/testdetail_graph.png" alt="graph"></div><!--** 추가수정 20210722 **-->
						<!-- gragh : e -->

						<!-- deta : s -->
						<div class="detail-data">
							<table cellpadding="0" cellspacing="0" border="0">

								<tr>
									<th>Raw</th>
									<th>I.Q. sd15</th>
									<th>Raw</th>
									<th>I.Q. sd15</th>
									<th>Raw</th>
									<th>I.Q. sd15</th>
									<th>Raw</th>
									<th>I.Q. sd15</th>
								</tr>
								<tbody class='ranking-tbody'>

								</tbody>


							</table>
						</div>
						<!-- deta : e -->

						<!-- rank : s -->
						<div class="detail-li width-full detail-best-div">
							<h2>Statistics of this test</h2>
							<div class="row detail-best-ranking-list"></div>
						</div>
						<!-- rank : e -->

						<!-- upload date : s -->
						<div class="detail-li width-full">
							<h2>Upload & Modification date</h2>
							<ul class="upload-date detail-history-list">
							</ul>
						</div>
						<!-- upload date : e -->

						<!-- grade : s -->
						<div class="detail-li">
							<div class="grade-ui">
								<h4 class="ic-star">
									<b>0.0</b> 0 users
								</h4>
<!-- 								<div class="grade-li">평점수치가 들어갑니다.</div> -->
							</div>
						</div>
						<!-- grade : e -->


						<!-- Recommended : s -->
						<div class="detail-li width-full swiper-container recommend-top-div">
							<h2>Recommended tests</h2>
							<div class="detail-recommend-list swiper-wrapper">
							</div>
						</div>
						<!-- Recommended : e -->

					</div>
				</div>
			</div>
			<!-- Detail : e -->

		</section>
		<!-- TestDetail_section : e -->

	</div>
</div>
<script>

	var is_open = true;
	$(function(){
		setInterval(function(){
			if(global_result_seq && $(".pop").css("display") != 'none' && is_open){
				var param = {
						"result_seq":global_result_seq
				}
				ajaxCallPost("/api/v1/iq/test/poling", param, function(res){
					if(res.data){
						$(".polinig-cnt").text("("+res.data.record + ")")

						if(res.data.record){
							$(".notice").show();
						}
					}
				})
			}else{
				$(".notice").hide();
			}
		}, 10000)
		$(".ic-close").click(function(){
			is_open = false;
			$(".notice").hide();
		})
	})


</script>
<div class="notice" style="display: none;    z-index: 99999999;"><b class="polinig-cnt">(n)</b> u sers are taking a test.<a class="ic-close"></a></div>


<!-- Popup Test : s -->
<div class="pop" style="display: none;">
	<h2>
		Basic IQ Test (Raven's Progressive Matrices)<a href="#" class="popClose"></a>
	</h2>
	<div class="pop-inner">
		<!-- Test : s -->
		<div class="test-ui">
			<div class="row">


				<!-- *0713추가사항* : start -->
				<div class="col-md-12 test-timer">
					<div class="test-ing"><span class="solve_cnt">0</span>/<span class="question-cnt">0</span></div>
					<div class="progress active">
						<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width:50%"></div>
					</div>
					<div class="ic-timer ing_time"></div>
				</div>
				<!-- *0713추가사항* : end -->

				<div class="question_list">
				</div>


				<!-- *0713추가사항* : start -->
				<div class="col-md-12 test-num">
					<ul>
						<li class="arrow"><a class="arrow-prev"></a></li>
						<li>
								<span class="num-m mobile-current-question"></span>
								<div class="test-num-list question-status">

								</div>
						</li>
						<li class="arrow"><a class="arrow-next"></a></li>
					</ul>


				</div>
				<!-- *0713추가사항* : end -->



				<div class="btn-box">
					<a class="btn-blue iq-test-submit">Submit</a>
				</div>


			</div>
		</div>
		<!-- Test : e -->



		<!-- Test Complete : s -->
		<div class="testComplete-ui testComplete-ui-submit" style="display: none">
			<div class="row">
				<div class="col-md-12">
					<img src="/resources/img/testComplete.png" alt="img" />
					<h3>
						Do you want to complete<br>this test and get the results?
					</h3>
					<div class="btn-box">

						<a href="#" class="btn-cancel">Cancel</a>
<!-- 						<a href="#" class="btn-submit btn-submit-add-temp">TemporarySave</a> -->
						<a href="#" class="btn-submit btn-submit-add">Submit</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Test Complete : e -->





	</div>
</div>
<!-- Popup Test : e -->
<div class="popBg" style="display: none;"></div>
