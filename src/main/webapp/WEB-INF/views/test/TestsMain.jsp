<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	$(function(){
		basicTestCnt();
		selectIqTestAct();
		selectIqTestUct();
		$(document).on("click", ".iq-test-item", function(res){
			var seq = $(this).attr("data-seq");
			location.href="/TestsDetail?seq="+seq;
		})
		$(document).on("click", ".btn_close", function(res){
			$(".notice_bar").hide();
		})
	})
	
	function basicTestCnt(){
		if(authToken){
			var param = {}
			ajaxCallPostToken("/api/v1/iq/test/1/cnt/one", authToken, param, function(res){
				if(res.success){
					$(".notice_bar").show();
				}
			})
		}else{
			$(".notice_bar").show();
		}
	}
	function selectIqTestAct(){
		var param = {
				   "write_type":"A"
		}
		ajaxCallPost("/api/v1/iq/test", param, function(res){
			var template = $.templates("#actList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".act-iq-test-list").html(htmlOutput);
		})
	}
	function selectIqTestUct(){
		var param = {
				   "write_type":"M"
		}
		ajaxCallPost("/api/v1/iq/test", param, function(res){
			var template = $.templates("#uctList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".uct-iq-test-list").after(htmlOutput);
		})
	}
</script>
<div id="container">
	<div class="wrapper">

		<!-- TestMain_section : s -->
		<section class="section section_testsMain">

			<!-- notice : s -->
			<div class="notice_bar notice_bar_blue" style="display: none;">
				<p>
					You have to get Basic IQ Test score first to take User-Created IQ Tests. <span class="btn_close"></span>
				</p>
			</div>
			<!-- notice : e -->

			<div class="row row-con">
				<div class="col-lg-12 m_tit">
					<h2>TESTS</h2>
				</div>
			</div>

			<!-- TP: s -->
			<div class="row row-con">
				<div class="col-lg-12">
					<h3>Test in Progress</h3>
				</div>
			</div>
			<div class="row row-con act-iq-test-list">
			</div>
			<!-- TP : e -->

			<!-- UCT : s -->
			<div class="row row-con">
				<div class="col-lg-12 uct-iq-test-list">
					<h3>User Created Tests (UCT)</h3>
				</div>
			</div>
			<!-- UCT : e -->


		</section>
		<!-- TestMain_section : e -->

	</div>
</div>
