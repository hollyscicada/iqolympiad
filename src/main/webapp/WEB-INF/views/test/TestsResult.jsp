<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	var global_result_seq = "${param.result_seq}";
	$(function(){
		var param = {
				"result_seq":global_result_seq
		}
		ajaxCallPostToken("/api/v1/iq/test/result", authToken, param, function(res){
			if(res.data){
				var obj = res.data;
				$(".member-name").text(obj.member_name);
				$(".result-iq").before("IQ "+obj.score+" (SD15)");
				$(".iq-sd-15").text(obj.score);
				$(".iq-sd-18").text(obj.score_18);
				$(".iq-sd-24").text(obj.score_24);
				$(".result-title").text(obj.title + " (Raven's Progressive Matrices)");
				$(".result-category-title").text(obj.category_title);
				
				
				var miunte = "";
				if(obj.left_minute && obj.left_minute > 0){
					miunte = obj.left_minute + " min ";	
				}
				var second = obj.left_second + " sec "; 
				
				var time = miunte + second;
				
				$(".result-left-second").text(time);
				$(".result-create").text(obj.create_dt);
				
				
				
				if(obj.write_type == 'M'){
					
					var top_a = 100-obj.top;
					$(".iq-top").text("top "+obj.top+"%");
					$(".iq-top2").text(top_a+"%");
				}else if(obj.write_type == 'A'){
					
					var top_a = 100-obj.persent;
					$(".iq-top").text("top "+top_a.toFixed(3)+"%");
					$(".iq-top2").text(obj.persent+"%");
				}
			}
		})
		
	})
</script>
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- TestResult_section : s -->
		<section class="section section_testsResult">

			<!-- Result: s -->
			<div class="row row-con">

				<div class="col-lg-12 m_tit"><h2>RESULT REPORT</h2></div>

				<div class="col-lg-12">
					<div class="testResult-ui">
						<h2>
							<span>Result</span>
						</h2>
						<div class="row">
							<div class="col-md-12">
								<h3>
									<span class="member-name"></span>, You have got the following score in this test.
								</h3>
							</div>
							<div class="col-md-5">
								<h1>
									<span class="result-iq">Percentile of the score is <b class="iq-top"></b></span>
								</h1>
							</div>
							<div class="col-md-7">
								<ul>
									<li>Test Title: <span class="result-title"></span></li>
									<li>Test Category: <span class="result-category-title"></span></li>
									<li>Time Taken: <span class="result-left-second"></span></li>
									<li>Submit Date: <span class="result-create"></span></li>
								</ul>
							</div>
							<div class="col-md-5 testResult-li">
								<div class="testResult-gragh graphfull-loading"><img src="/resources/img/testresult_graph.svg" alt="graph"></div>
								<div class="btn-sns-box">
									<a class="btn-sns ic-twitter"></a> <a class="btn-sns ic-facebook"></a>
								</div>
							</div>
							<div class="col-md-7 testResult-li">
								<ul>
									<li><h3>
											Your Score is better than <span class="iq-top2"></span>
										</h3></li>
									<li><b>IQ Olympiad is adopting Standard Deviation 15.<br>If you convert to another SD, it can be expressed by the score below.
									</b></li>
									<li>Standard Deviation 15 (Wechsler) :<span class="iq-sd-15"></span></li>
									<li>Standard Deviation 16 (Stanford-Binet) :<span class="iq-sd-18"></span></li>
									<li>Standard Deviation 24 (Cattell) :<span class="iq-sd-24"></span></li>
								</ul>

								<div class="btn-sns-box_m">
									<a class="btn-sns ic-twitter"></a> <a class="btn-sns ic-facebook"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Result: : e -->

		</section>
		<!-- TestResult_section : e -->

	</div>
</div>
