<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<script>
		$(function(){
			
	            $("#id").keydown(function(key) {
	                if (key.keyCode == 13) {
	                	$("#pw").focus();
	                }
	            });
	            $("#pw").keydown(function(key) {
	                if (key.keyCode == 13) {
	                	$(".loginbtn").trigger("click");
	                }
	            });


			
			$(".loginbtn").click(function(){
				var id = $("#id").val();
				var pw = $("#pw").val();
				
				var param = {
						"id" : id,
						"pw" : pw
				}
				
				ajaxCallPost("/supervise/api/v1/login", param, function(res){
					if(res.success){
						location.href="/supervise";
					}else{
						alert("Please check your ID or password.")
					}
				},function(){})
			})
			$("#id").focus();
		})
	</script>
	<div class="error-pagewrap">
		<div class="error-page-int">
			<div class="text-center m-b-md custom-login">
				<h3><span>IQ-Olympiad ADMIN SYSTEM</span></h3>
			</div>
			<div class="content-error">
				<div class="hpanel">
                    <div class="panel-body">
                        <form action="#" id="loginForm">
                            <div class="form-group">
                                <label class="control-label" for="username">ID</label>
                                <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="id" id="id" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="pw">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="pw" id="pw" class="form-control">
                            </div>
                            <button type="button" class="btn btn-success btn-block loginbtn">Login</button>
                        </form>
                    </div>
                </div>
			</div>
			<div class="text-center login-footer">
				<p>COPYRIGHT © 2021 IQ-Olympiad. ALL RIGHTS RESERVED.</p>
			</div>
		</div>   
    </div>
  