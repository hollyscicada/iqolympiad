<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/jsrender.jsp"%>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
 
    // 파일 리스트 번호
    var fileIndex = 0;
    // 등록할 전체 파일 사이즈
    var totalFileSize = 0;
    // 파일 리스트
    var fileList = new Array();
    // 파일 사이즈 리스트
    var fileSizeList = new Array();
    // 등록 가능한 파일 사이즈 MB
    var uploadSize = 50;
    // 등록 가능한 총 파일 사이즈 MB
    var maxUploadSize = 500;
 
    $(function (){
        // 파일 드롭 다운
        fileDropDown();
    });
 
    // 파일 드롭 다운
    function fileDropDown(){
        var dropZone = $("#dropZone");
        //Drag기능 
        dropZone.on('dragenter',function(e){
            e.stopPropagation();
            e.preventDefault();
            // 드롭다운 영역 css
            dropZone.css('background-color','#E3F2FC');
        });
        dropZone.on('dragleave',function(e){
            e.stopPropagation();
            e.preventDefault();
            // 드롭다운 영역 css
            dropZone.css('background-color','#FFFFFF');
        });
        dropZone.on('dragover',function(e){
            e.stopPropagation();
            e.preventDefault();
            // 드롭다운 영역 css
            dropZone.css('background-color','#E3F2FC');
        });
        dropZone.on('drop',function(e){
            e.preventDefault();
            // 드롭다운 영역 css
            dropZone.css('background-color','#FFFFFF');
            
            var files = e.originalEvent.dataTransfer.files;
            if(files != null){
                if(files.length < 1){
                    alert("폴더 업로드 불가");
                    return;
                }
                selectFile(files)
            }else{
                alert("ERROR");
            }
        });
    }
 
    // 파일 선택시
    function selectFile(files){
    	
    	if(confirm("파일 업로드를 하시겠습니까?")){
    		
	        // 다중파일 등록
	        if(files != null){
	            for(var i = 0; i < files.length; i++){
	                // 파일 이름
	                var fileName = files[i].name;
	                var fileNameArr = fileName.split("\.");
	                // 확장자
	                var ext = fileNameArr[fileNameArr.length - 1];
	                // 파일 사이즈(단위 :MB)
	                var fileSize = files[i].size/1024/1024;
	                
	                if($.inArray(ext, ['exe', 'bat', 'sh', 'java', 'jsp', 'html', 'js', 'css', 'xml']) >= 0){
	                    // 확장자 체크
	                    alert("등록 불가 확장자");
	                    break;
	                }else if(fileSize > uploadSize){
	                    // 파일 사이즈 체크
	                    alert("용량 초과\n업로드 가능 용량 : " + uploadSize + " MB");
	                    break;
	                }else{
	                    // 파일 배열에 넣기
	                    fileList[0] = files[i];
	                }
	                uploadFile();
	            }
	        }else{
	            alert("ERROR");
	        }
    	}
    }
    // 파일 등록
    function uploadFile(){
        // 등록할 파일 리스트
        var uploadFileList = Object.keys(fileList);
 
        // 파일이 있는지 체크
        if(uploadFileList.length == 0){
            // 파일등록 경고창
            alert("파일이 없습니다.");
            return;
        }
        
            
        // 등록할 파일 리스트를 formData로 데이터 입력
        var formData = new FormData($('#guploadForm')[0]);
            formData.append('files', fileList[uploadFileList[0]]);
        $.ajax({
            url:"/api/file/upload_multipart/front",
            data:formData,
            type:'POST',
            enctype:'multipart/form-data',
            processData:false,
            contentType:false,
            dataType:'json',
            cache:false,
            success:function(res){
                if(res.success){
			        var htmlOutput = $("#tempFileUpladDiv").html(); // <!-- 렌더링 진행 -->
			        htmlOutput = htmlOutput.replace("{img_url}",res.img_url)
			        if($("#file-div-all .file-div-item").length == 0){
			        	$("#file-div-all").html(htmlOutput)
			        }else{
			        	$("#file-div-all .file-div-item").eq(0).before(htmlOutput)
			        }
                }
            }
        });
    }
</script>
<div id="tempFileUpladDiv" style="display: none">
	<div style="width: 20%; display: inline-block; margin: 5px 5px 5px 0px" class="file-div-item" data-file-seq="" data-show-yn="Y">
                                 <img src="{img_url}" style="width: 100%">
                                 	<div><button type="button" class="file-del-btn btn btn-custon-four btn-danger" style="margin:10px 0px 20px 0px;width: 100%;">삭제하기</button></div>
                               </div>
</div>

