<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@include file="/WEB-INF/views/admin/common/libTable.jsp"%> 
<script>
	$(function(){
			$(".btn-danger").click(function(){
				if(confirm("Proceed to delete?")){
					var seq = "${detail.member_seq }";
					ajaxCallGet("/supervise/admin/v1/member/remove?seq="+seq, function(res){
						if(res.success){
							alert("The task has been completed.");
							location.href="/supervise/member/list"
						}
					})
				}
			})
				setTimeout(function(){
				$("#table").fadeIn(1000);
			},500)
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
						<div class="sparkline13-hd">
							<div class="main-sparkline13-hd">
								<h1 style="margin-bottom:20px;">Member <span class="table-project-n">Data</span> Table</h1>
                            </div>
                        </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
								<div class="row">
									<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12 text-center" style="width:100%;">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th class="text-center">Member</th>
													<th class="text-center">Result</th>
												</tr>
			                                    <tr>
			                                        <td>Join Type</td>
			                                        <td>
			                                        	  <c:choose>
	                                                		<c:when test="${detail.login_type eq 'G' }">Google</c:when>
	                                                		<c:when test="${detail.login_type eq 'F' }">Facebook</c:when>
	                                                	</c:choose>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td>Name</td>
			                                         <td>${detail.name }</td>
			                                    </tr>
			                                     <tr>
			                                        <td>Email</td>
			                                         <td>${detail.email }</td>
			                                    </tr>
			                                     <tr>
			                                        <td>Sex</td>
			                                         <td>
			                                         	<c:choose>
	                                                		<c:when test="${detail.sex eq 'M' }">Male</c:when>
	                                                		<c:when test="${detail.sex eq 'W' }">Female</c:when>
	                                                	</c:choose>
			                                         </td>
			                                    </tr>
			                                     <tr>
			                                        <td>Age</td>
			                                         <td>${detail.age }</td>
			                                    </tr>
			                                     <tr>
			                                        <td>Country</td>
			                                         <td>${detail.country_name }</td>
			                                    </tr>
			                                     <tr>
			                                        <td>IQ</td>
			                                         <td>${detail.iq }</td>
			                                    </tr>
			                                     <tr>
			                                        <td>IQ Test</td>
			                                         <td></td>
			                                    </tr>
			                                    <tr>
			                                        <td>Create date</td>
			                                        <td>${detail.create_dt_yyyymmdd }</td>
			                                    </tr>
			                                </tbody>
			                            </table>
										<button type="button" class="btn btn-custon-four btn-danger" style="float: right;margin-top: 30px;">Delete</button>
										
									</div>
								</div>
							</form>
						</div>
						
						
						
						<!-- 내가 참여한 대회 리스트 -->
						<div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>IQ Test Data <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
						<div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control dt-tb">
											<option value="">Export Basic</option>
											<option value="all">Export All</option>
											<option value="selected">Export Selected</option>
										</select>
                                    </div>
                                    <table id="table" style="display: none" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Test Name</th>
                                                <th>Score</th>
                                                <th>Create date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${detail_member }" var="item" >
                                           	   <tr>
	                                                <td>${item.rownum }</td>
	                                                <td>${item.name }</td>
	                                                <td>0</td>
	                                                <td>${item.create_dt_yyyymmdd }</td>
	                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
						<!-- 내가 참여한 대회 리스트 -->
						

						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
