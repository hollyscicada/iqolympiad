<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<script>
//0~9까지의 난수
function random(n1, n2) {
  return parseInt(Math.random() * (n2 -n1 +1)) + n1;
};
//인증번호를 뽑을 난수 입력 n 5이면 5자리
function randomNumber(n) {
  var value = "";
  for(var i=0; i<n; i++){
      value += random(0,9);
  }
  return value;
};
	$(function(){
		$(".submit-btn").click(function(){
			var id = $("input[name=id]");
			var pw = $("input[name=pw]");
			var name = $("input[name=name]");
			var phone = $("input[name=phone]");
			var write_by = $("input[name=write_by]");
			
			
			
			
			var param = {
					"id" : id.val(),
					"pw" : pw.val(),
					"name" : name.val(),
					"phone" : phone.val(),
					"write_by": write_by.val()
			}
			
			ajaxCallPost("/supervise/admin/v1/admin/create", param, function(res){
				if(res.success){
					alert("작업이 완료되었습니다.");
					location.href="/supervise/admin/list"
				}
			},function(){})
		})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
					 <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Admin <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
							<input type="hidden" name="write_by" value="${sessionScope.admin_seq}">
								<div class="row">
									<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input name="id" type="text" class="form-control"
												placeholder="ID">
										</div>
										<div class="form-group">
											<input name="pw" type="password" class="form-control"
												placeholder="Password">
										</div>
										<div class="form-group">
											<input name="name" type="text" class="form-control"
												placeholder="Name">
										</div>
										<div class="form-group">
											<input name="phone" type="number" class="form-control"
												placeholder="Phone">
										</div>
									</div>


								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="payment-adress">
											<button type="button"
												class="btn btn-primary waves-effect waves-light submit-btn">Add</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
