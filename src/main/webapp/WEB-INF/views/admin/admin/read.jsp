<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
	$(function(){
			$(".btn-success").click(function(){
				if(confirm("Proceed to edit?")){
					var seq = "${detail.admin_seq }";
					location.href="/supervise/admin/v1/admin/update/"+seq;
				}
			})
			$(".btn-danger").click(function(){
				if(confirm("Proceed to delete?")){
					var seq = "${detail.admin_seq }";
					ajaxCallGet("/supervise/admin/v1/admin/remove?seq="+seq, function(res){
						if(res.success){
							alert("The task has been completed.");
							location.href="/supervise/admin/list"
						}
					})
				}
			})
	})
</script>

<!-- Static Table Start -->
<div class="all-content-wrapper"
	style="min-height: 900px; background: white;">



	<div id="myTabContent" class="tab-content custom-product-edit"
		style="padding: 0px 50px 0 50px">
		<div class="product-tab-list tab-pane fade active in" id="description">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="review-content-section">
					
						<div class="sparkline13-hd">
							<div class="main-sparkline13-hd">
								<h1 style="margin-bottom:20px;">Admin <span class="table-project-n">Data</span> Table</h1>
                            </div>
                        </div>
					
						<div id="dropzone1" class="pro-ad">
							<form class="dropzone dropzone-custom needsclick add-professors">
								<div class="row">
									<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12 text-center" style="width:100%;">
										<table id="user" class="table table-bordered table-striped x-editor-custom">
			                                <tbody>
			                                	<tr>
													<th class="text-center">Admin</th>
													<th class="text-center">Result</th>
												</tr>
			                                    <tr>
			                                        <td>ID</td>
			                                        <td>${detail.id }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>Name</td>
			                                         <td>${detail.name }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>Phone</td>
			                                         <td>${detail.phone }</td>
			                                    </tr>
			                                    <tr>
			                                        <td>Create Date</td>
			                                        <td>${detail.create_dt_yyyymmdd }</td>
			                                    </tr>
			                                </tbody>
			                            </table>
										<button type="button" class="btn btn-custon-four btn-success" style="margin-right:10px">Modify</button>
										<button type="button" class="btn btn-custon-four btn-danger" style="">Delete</button>
										
									</div>
								</div>
							</form>
						</div>
						

						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Static Table End -->
