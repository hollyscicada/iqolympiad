 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
 <%@ include file="/WEB-INF/views/common/jstl.jsp"%>
 <%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
 <style>
 	@media (max-width:1199px) {
 		.mobile-hide{display: none}
 	}
 </style>
 <script>
 	var global_searchType = "";
 	$(function(){
 		
 		
 		$(".search").click(function(){
 			$(".search").removeClass("active");
 			$(this).addClass("active");
 			var searchType = $(this).attr("data-search-type");
 			global_searchType = searchType;
 			if(searchType == 'TOTAL'){
 				$(".ranking-select").css("width","100%");
 				$(".search-select").hide();
 	 			selectRankingList(searchType, "");
 			}else if(searchType == 'COUNTRY'){
 				var param = {}
 				ajaxCallPost("/api/v1/country", param, function(res){
 					var template = $.templates("#countryList"); // <!-- 템플릿 선언
 			        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
 			        $(".search-select").html(htmlOutput);
 			        
 			        
 			        $(".ranking-select").css("width","49%");
 			        $(".search-select").show();
 				})
 			}else if(searchType == 'AGE'){
 				
 				$(".ranking-select").css("width","49%");
 				$(".search-select").show();
 				
 				var template = $.templates("#ageList"); // <!-- 템플릿 선언
 		        var htmlOutput = template.render(); // <!-- 렌더링 진행 -->
 		        $(".search-select").html(htmlOutput);
 			}
 			
 			
 			var searchs = $(".search2");
 			$(".search2").removeClass("selected");
 			for(var i = 0 ; i < searchs.length ; i++){
 				var tsearchType = searchs.eq(i).attr("data-search-type");
 				
 				if(tsearchType == searchType){
 					searchs.eq(i).addClass("selected");
 					$("ul.ranking-select").children('.init').html(searchs.eq(i).html());
 				}
 			}
 			
 		})
 		$(".search-select").change(function(){
 			var searchValue = $(this).val();
 			
 			if(searchValue){
	 			selectRankingList(global_searchType, searchValue);
 			}else{
 				alert("Please apply a different search filter.")
 			}
 		})
 		
 		selectRankingList("TOTAL", "")
 	})
 	function selectRankingList(type, value){
 		var param = {
 			    "type":type,
 			  	"type_val":value
 			}
 		ajaxCallPost("/api/v1/ranking", param, function(res){
 			var template = $.templates("#rankingList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
	        $(".tbody").html(htmlOutput);
 		})
 	}
 </script>
<div id="container">
	<div class="wrapper wrapper-70">

		<!-- ranking_section : s -->
		<section class="section section_ranking">
			<div class="row row-con">

				<div class="col-lg-12 m_tit">
					<h2>WORLD IQ RANKING</h2>
					<br>
				</div>

				<!-- ranking : s -->
				<div class="col-md-12">
					<div class="ranking-ui">
						<h2>
							<span>World IQ Hall of fame</span>
							<div>
								<!--* 셀렉박스 추가 : s *-->
									<select class="selectmenu-select search-select" style="display:none; ">
									</select>
								<!--* 셀렉박스 추가 : e *-->

								<ul class="ranking-select-menu">
									<li class="search active" data-search-type="TOTAL" data-search-value=""><span>World IQ</span></li>
									<li class="search" data-search-type="COUNTRY" data-search-value="1"><span>Country</span></li>
									<li class="search" data-search-type="AGE" data-search-value="2"><span>By Age</span></li>
								</ul>

								<ul class="ranking-select" style="width: 100%">
									<li class="init"><span>World IQ</span></li>
									<li class="search2 selected" data-search-type="TOTAL" data-search-value="" ><span>World IQ</span></li>
									<li class="search2" data-search-type="COUNTRY" data-search-value="1"><span>Country</span></li>
									<li class="search2" data-search-type="AGE" data-search-value="2"><span>By Age</span></li>
								</ul>
							</div>



						</h2>
						<div class="ranking-data">
							<table cellpadding="0" cellspacing="0" border="0">
								<thead>
									<tr>
										<th><span>Ranking</span></th>
										<th><span>Last 24h</span></th>
										<th><span>Name</span></th>
										<th><span>IQ (SD 15)</span></th>
										<th><span>Num. of Tests (Problems)</span></th>
										<th class="mobile-hide"><span>Country</span></th>
									</tr>
								</thead>
								<tbody class="tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- ranking 2 : e -->
			</div>


		</section>
		<!-- ranking_section : e -->

	</div>
</div>
<script>
	$("ul.ranking-select").on(
			"click",
			".init",
			function() {
				$(this).closest("ul.ranking-select").children('li:not(.init)')
						.toggle();
			});

	var allOptions = $("ul.ranking-select").children('li:not(.init)');

	$("ul.ranking-select").on("click", "li:not(.init)", function() {
		allOptions.removeClass('selected');
		$(this).addClass('selected');
		$("ul.ranking-select").children('.init').html($(this).html());

		allOptions.toggle();
		
		
		
		
		
			
		var searchType = $(this).attr("data-search-type");
		global_searchType = searchType;
		if(searchType == 'TOTAL'){
			
			$(".ranking-select").css("width","100%");
			$(".search-select").hide();
			
 			selectRankingList(searchType, "");
		}else if(searchType == 'COUNTRY'){
			var param = {}
			ajaxCallPost("/api/v1/country", param, function(res){
				var template = $.templates("#countryList"); // <!-- 템플릿 선언
		        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
		        $(".search-select").html(htmlOutput);
		        
		        $(".ranking-select").css("width","49%");
		        $(".search-select").show();
			})
		}else if(searchType == 'AGE'){
			
			$(".ranking-select").css("width","49%");
			$(".search-select").show();
			
			
			var template = $.templates("#ageList"); // <!-- 템플릿 선언
	        var htmlOutput = template.render(); // <!-- 렌더링 진행 -->
	        $(".search-select").html(htmlOutput);
		}
		
		
		var searchs = $(".search");
		$(".search").removeClass("active");
		for(var i = 0 ; i < searchs.length ; i++){
			var tsearchType = searchs.eq(i).attr("data-search-type");
			
			if(tsearchType == searchType){
				searchs.eq(i).addClass("active");
			}
		}

	});
</script>
