<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<!-- footer : s -->
<footer id="footer" class="f_bg-white">
	<div class="container-fluid">
		<div class="info">
			<ul>
				<li class="copy">© 2021 IQ Olympiad. All rights reserved.</li>
				<li class="by">Powered by IQ OLYMPIAD FOUNDATION</li>
			</ul>
		</div>
		<!-- info : e -->
	</div>
</footer>
<!-- footer : e -->