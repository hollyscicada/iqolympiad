<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<script>
	$(function(){
		$(".logout").click(function(){
				location.href="/logout";
		})
		$(".member-update-btn").click(function(){
			location.href="/member/mypage";
		})
		$(".ic-user").click(function(){
			$(".my-list").show();
		})
		$(".terms").click(function(){
			location.href="/terms";
		})
		if(location.pathname.indexOf("dashboardMain") > -1 || location.pathname.indexOf("dashboardDetail") > -1 || location.pathname.indexOf("dashboardCertification") > -1){
			$(".gnb_submenu").show();
		}else{
			$(".dashboard-header").hover(function(){
				$(".gnb_submenu").show()
			}, function(){
				$(".gnb_submenu").hide()
			})

			$(".dashboard-header").click(function(){
				if($(".gnb_submenu").css("display") == 'none'){
					$(".gnb_submenu").show();
				}else{
					$(".gnb_submenu").hide();
				}
			})
		}
	})
	$(function(){
		var global_menu_tab = $(".dashboard-menu-li");
		for(var i = 0 ; i < global_menu_tab.length ; i++){
			var data_url = global_menu_tab.eq(i).attr("data-url").split("|");
			for(var x = 0 ; x < data_url.length ; x++){
				if(location.pathname.indexOf(data_url[x]) > -1 && data_url[x]){
					global_menu_tab.removeClass("active");
					global_menu_tab.eq(i).addClass("active");
				}
			}
		}
		var global_menu_tab = $(".dashboard-sub-menu-li");
		for(var i = 0 ; i < global_menu_tab.length ; i++){
			var data_url = global_menu_tab.eq(i).attr("data-url").split("|");
			for(var x = 0 ; x < data_url.length ; x++){
				if(location.pathname.indexOf(data_url[x]) > -1 && data_url[x]){
					global_menu_tab.removeClass("active");
					global_menu_tab.eq(i).addClass("active");
				}
			}
		}
	})
	function goMain(){
		localStorage.setItem("goMain", "Y");
		location.href="/";
	}
</script>
<header id="header" class="">
	<div class="container-fluid">
		<h1 class="logo">
			<a href="javascript:goMain()"> <img src="/resources/img/iq_test_logo.svg" alt="logo">
			</a>
		</h1>
		<button type="button" aria-expanded="false" class="btn-open-nav">
			<span></span> <span></span> <span></span> <span></span>
		</button>
		<nav>
			<span class="btn-close-nav"></span>
			<ul class="gnb gnb-left">
				<li class="has-child-menu dashboard-header"><a href="/dashboardMain" class="dashboard-menu-li" data-url="dashboardMain|dashboardDetail|dashboardCertification">Dashboard</a>
					<div class="gnb_submenu">
						<ul>
							<li class="dashboard-sub-menu-li" data-url="dashboardMain"><a href="/dashboardMain">Main</a></li>
							<li class="dashboard-sub-menu-li" data-url="dashboardDetail"><a href="/dashboardDetail">IQ Details</a></li>
							<li class="dashboard-sub-menu-li" data-url="dashboardCertification"><a class="comingsoon-btn">Certification</a></li>
						</ul>
					</div>
				</li>
				<li class="has-child-menu"><a href="/TestsMain" class="dashboard-menu-li" data-url="TestsMain|TestsDetail">Tests</a></li>
				<li class="has-child-menu"><a href="/ranking" class="dashboard-menu-li" data-url="ranking">Ranking</a></li>
<!-- 				<li class="has-child-menu"><a class="comingsoon-btn">Forum</a></li> -->
			</ul>

			<ul class="gnb-member">
				<c:choose>
					<c:when test="${empty sessionScope.MEMBER.member_seq }">
						<li class="rightline header-login-btn"><a href="/login">Sign in</a></li>
						<li class="header-sign-btn"><a href="/login?type=sign">Sign up</a></li>
					</c:when>
					<c:otherwise>
						<li class="user-img">

							<span class="ic-user"><img src="${sessionScope.MEMBER.img_url }" onerror="this.src='/resources/img/user_img.png'" alt="logo"></span>
							<ul class="my-list" style="z-index: 99999">
								<li class="member-update-btn">Account setting</li>
 								<!--li class="comingsoon-btn">Customer Support</li> -->
								<li class="terms">Terms</li>
								<li class="logout">Logout</li>
							</ul>

							<!--* 포인트 추가 : s *-->
							<div class="my-point">
								<c:if test="${!empty sessionScope.MEMBER.point}">
								<fmt:formatNumber value="${sessionScope.MEMBER.point }" pattern="#,###" />
								</c:if>
							</div>
							<!--* 포인트 추가 : e *-->
						</li>
					</c:otherwise>
				</c:choose>
			</ul>

		</nav>
		<div class="nav-dimmed"></div>
	</div>
</header>
