<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script>
var login_yn = "${sessionScope.admin_seq}";
if(!login_yn){
	alert("Please sign in.");
	location.href="/supervise";
}


	$(function(){
		var global_menu_tab = $(".global-menu-tab");
		for(var i = 0 ; i < global_menu_tab.length ; i++){
			var data_url = global_menu_tab.eq(i).attr("data-url").split("|");
			for(var x = 0 ; x < data_url.length ; x++){
				if(location.pathname.indexOf(data_url[x]) > -1 && data_url[x]){
					global_menu_tab.removeClass("active");
					global_menu_tab.find("a").attr("aria-expanded", "true");
					global_menu_tab.find("ul").removeClass("show");
					
					global_menu_tab.eq(i).addClass("active");
					global_menu_tab.eq(i).find("a").attr("aria-expanded", "true");
					global_menu_tab.eq(i).find("ul").addClass("show");
				}
			}
		}
	})
</script>
<style>
	.go-detail-item{
		cursor: pointer;
	}
	.payment-adress button{
		    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
    float: right;
    margin-top: 30px;
    margin-bottom: 30px;
	}
</style>
<!-- 레프트 메뉴 -->
 <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="/supervise" style="width:80px;background-size: 80px auto;"></a>
                <strong><a href="/supervise"><img src="/resources/admin/img/logo/logosn.png" alt="" /></a></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                    
                    
	                        <li class="global-menu-tab" data-url="admin">
	                            <a class="has-arrow" href="/supervise/admin/list">
									   <span class="educate-icon educate-home icon-wrap"></span>
									   <span class="mini-click-non">Admin</span>
									</a>
	                            <ul class="submenu-angle" aria-expanded="true" style="display: none">
	                                <li><a title="Dashboard v.1" href="/supervise/admin/list"><span class="mini-sub-pro">Admin List</span></a></li>
	                            </ul>
	                        </li>
	                         <li class="global-menu-tab" data-url="member">
	                            <a class="has-arrow" href="/supervise/member/list">
									   <span class="educate-icon educate-form icon-wrap"></span>
									   <span class="mini-click-non">Member</span>
									</a>
									 <ul class="submenu-angle" aria-expanded="true" style="display: none">
	                                <li><a title="Dashboard v.1" href="/supervise/member/list"><span class="mini-sub-pro">Member List</span></a></li>
	                            </ul>
	                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
<!-- 레프트 메뉴 -->
<script>
// $(".submenu-angle").show();
</script>
<!-- 상단메뉴 -->
		<div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="/supervise"><img class="main-logo" src="/resources/images/logo_dark.svg" alt="" style="height: 60px;
    padding: 20px;" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12"style="width: 100%;">
                                        <div class="header-top-menu tabl-d-n">
                                            <ul class="nav navbar-nav mai-top-nav" style="float: right">
                                                <li class="nav-item"><a href="#" class="nav-link" style="color:black;font-size: 12px;">${sessionScope.name}님</a></li>
                                                <li class="nav-item"><a href="/supervise/logout" class="nav-link" style="color:#8d9498;font-size: 12px;"">Logout</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a data-toggle="collapse" data-target="#Charts" href="/supervise/admin/list">Admin<span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul class="collapse dropdown-header-top">
                                                <li><a href="/supervise/admin/list">Admin List</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li><a data-toggle="collapse" data-target="#Charts" href="/supervise/member/list">Member<span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                        	 <ul class="collapse dropdown-header-top">
                                                <li><a href="/supervise/member/list">Member List</a></li>
                                            </ul>
                                        </li>
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- Mobile Menu end -->

<!-- 상단메뉴 -->