<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="today" class="java.util.Date" />
<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" var="nowDate"/>

<!DOCTYPE html>
<html lang="ko">
<head>
	
	
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Generator" content="EditPlus®">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="mvp">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="EditPlus">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<link href="/resources/img/favicon.png" rel="shortcut icon" type="image/x-icon">
	<title>IQ Olympiad</title>
	
	<!-- css -->
	<link rel="stylesheet" type="text/css" href="/resources/css/vendor/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/resources/css/vendor/aos.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Bebas+Neue&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/resources/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/resources/css/layout.css">
	<link rel="stylesheet" type="text/css" href="/resources/css/sub.css">
	<link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.custom.css">
	<link rel="stylesheet" href="/resources/css/swiper.min.css" />

	<!-- js -->
	<script type="text/javascript" src="/resources/js/vendor/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="/resources/js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="/resources/js/vendor/aos.js"></script>
	<script type="text/javascript" src="/resources/js/vendor/jquery.parallax.min.js"></script>
	<script type="text/javascript" src="/resources/js/custom.js" charset="utf-8"></script>
	
	
	
	
	
	
    <script src="/resources/js/common.js?v=${nowDate}"></script>
	<script src="/resources/js/commonFront.js?v=${nowDate}"></script>
	<script src="/resources/js/commonAjax.js?v=${nowDate}"></script>
	<script src="https://www.jsviews.com/download/jsrender.js"></script>
	
	
	

</head>

<body class="">
    <!-- Main wrapper  -->
  	<main role="main">
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="content" />
		<tiles:insertAttribute name="footer" />
	</main>
</body>

</html>
