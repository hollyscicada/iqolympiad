$(function () {
    'use strict';

    // 해상도 분기점 전역변수
    var $resolution_pc = 1200;
    var $resolution_tablet = 992;
    var $resolution_mobile = 768;

    // 초기화
    aos_init();
    main_hero_init();
    navigation_init();
    mobile_btn_init();
    parallax_init();
    navi_init();

    // 스크롤 이벤트
    $(window).on('scroll', function () {
        header_scroll();
        //subhero_scroll();
    }).scroll();

    // AOS 실행
    function aos_init() {
        AOS.init({
            once: true
        });
    }

    // 메인 롤링이미지
    function main_hero_init() {
        if ($('#main-hero').length) {
            var $interval = ( $('#main-hero').data('interval') ) ? $('#main-hero').data('interval') : 4000;

            $('#main-hero').slick({
                autoplay: true,
                autoplaySpeed: $interval,
                speed: 600,
                arrows: true,
                dots: true,
                lazyLoad: 'ondemand',
                prevArrow: '<button type="button" class="slick-prev"><i class="ion-ios-arrow-round-back"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="ion-ios-arrow-round-forward"></i></button>'
            });
        }
    }


    // 상단 내비게이션 초기화
    function navigation_init() {
        var $header = $('#header');
        var $openBtn = $header.find('.btn-open-nav');
        var $closeBtn = $header.find('.btn-close-nav');
        var $navDimmed = $header.find('.nav-dimmed');
        var $maxHeight = 0;
        var $bg = $header.find('.subnav-bg');

        $header.find('.gnb > li').each(function () {
            if ($(this).find('.subnav').length) $(this).addClass('has-child-menu');
        });

        function nav_open() {
        	
        	$(".gnb-member").show();
        	
        	$('html, body').css({'overflow': 'hidden', 'height': '100%'});
        	$("html").on('scroll touchmove mousewheel', function(e){
        		e.preventDefault();
        		e.stopPropagation(); 
        		return false;
    		})
        	
        	
            $openBtn.attr('aria-expanded', 'true');
            $navDimmed.stop().fadeIn(350);
        }

        function nav_close() {
        	$(".gnb-member").hide();
        	$('html, body').css({'overflow': 'unset', 'height': 'unset'});
        	$("body").off('scroll touchmove mousewheel'); 

        	
        	$openBtn.attr('aria-expanded', 'false');
            $navDimmed.stop().fadeOut(350);
            

            
        }

        
        $openBtn.on('click', function () {
        	
        	
			$(".nav-dimmed").css("height", document.body.offsetHeight+"px");
            nav_open();
        });

        $closeBtn.on('click', function () {
            nav_close();
        });

        $navDimmed.on('click', function () {
            nav_close();
        });

        $header.find('.gnb > li > a').on('mouseenter', function () {
            if ($(window).width() >= $resolution_tablet) {
                $maxHeight = 0;
                $header.addClass('is-active');
                $header.find('.gnb > li').each(function () {
                    if ($(this).find('.subnav').outerHeight() >= $maxHeight) $maxHeight = $(this).find('.subnav').outerHeight();
                });

                $header.find('.subnav').stop().slideDown(300);
                $bg.height($maxHeight).stop().slideDown(300);
            }
        });

        $header.on('mouseleave', function () {
            $header.removeClass('is-active');
            $header.find('.subnav').stop().slideUp(300);
            $bg.stop().slideUp(300);
        });

        $header.find('.gnb > li > a').on('click', function (e) {
            if ($(window).width() < $resolution_tablet) {
                if ($(this).siblings('.subnav').length) {
                    e.preventDefault();

                    if (!$(this).parent().hasClass('is-open')) {
                        $header.find('.gnb > li.is-open').removeClass('is-open');
                        $header.find('.subnav').stop().slideUp(300);
                        $(this).parent().addClass('is-open');
                        $(this).siblings('.subnav').stop().slideDown(300);
                    } else {
                        $(this).parent().removeClass('is-open');
                        $(this).siblings('.subnav').stop().slideUp(300);
                    }
                }
            }
        });
    }

    // 모바일 버튼 클릭이벤트
    function mobile_btn_init() {
        $('.btn').on({
            'touchstart': function () {
                $(this).addClass('active');
            },
            'touchend': function () {
                $(this).removeClass('active');
            }
        });
    }

    // sticky 헤더

    function header_scroll() {
        var $header = $('#header');
        var $scrTop = $(window).scrollTop();
        var $pos = ($('#main-hero').length) ? $('#main-hero').outerHeight() - $header.outerHeight() : $('#sub-hero').outerHeight() - $header.outerHeight();

        if ($scrTop >= $pos) {
            if (!$header.hasClass('sticky')) $header.addClass('sticky');
        } else {
            $header.removeClass('sticky');
        }
    }

    // 서브페이지 드롭다운 내비게이션 연동
    function navi_init() {
        var $gnb = $('#header .gnb');
        var $dropdown = [];
        var $url = $(location).attr('href');
        var $idx_main = 0;
        var $idx_sub = 0;
        var $dropdown_html = [];

        $('.dropdown-nav-section .dropdown-wrap').each(function (i) {
            $dropdown[i] = $(this);
            i++;
        });

        $gnb.children().each(function () {
            if ($url.indexOf($(this).find('>a').attr('href')) > -1) {
                $idx_main = $(this).index();
                $(this).addClass('is-active is-open');
                $(this).find('.subnav').addClass('visible');
            }

            $(this).find('.subnav > li').each(function () {
                if ($url.indexOf($(this).find('>a').attr('href')) > -1) {
                    $idx_main = $(this).closest('.has-child-menu').index();
                    $idx_sub = $(this).index();
                    $(this).addClass('is-active');
                    $(this).closest('.has-child-menu').addClass('is-active');
                }
            });
        });

        if ($('.dropdown-nav-section .dropdown-wrap').length) {
            $dropdown_html[0] = '';
            $dropdown_html[1] = '';

            $gnb.children().each(function () {
                var $link = $(this).find('>a').attr('href');
                var $name = $(this).find('>a').text();

                $dropdown_html[0] += '<li><a href="' + $link + '">' + $name + '</a></li>';
            });

            $gnb.children().eq($idx_main).find('.subnav > li').each(function () {
                var $link = $(this).find('>a').attr('href');
                var $name = $(this).find('>a').text();

                $dropdown_html[1] += '<li><a href="' + $link + '">' + $name + '</a></li>';
            });

            $dropdown[0].find('.dropdown').append($dropdown_html[0]);
            $dropdown[1].find('.dropdown').append($dropdown_html[1]);

            $dropdown[0].find('.dropdown').children().eq($idx_main).addClass('is-active');
            $dropdown[1].find('.dropdown').children().eq($idx_sub).addClass('is-active');

            $dropdown[0].find('>a').text($dropdown[0].find('.is-active').text());
            $dropdown[1].find('>a').text($dropdown[1].find('.is-active').text());

            function dropdown_close() {
                $('.dropdown-nav-section .dropdown-wrap > a').attr('aria-expanded', 'false');
                $('.dropdown-nav-section .dropdown-wrap > .dropdown').stop().slideUp(300);
            }

            $('.dropdown-nav-section .dropdown-wrap > a').on('click', function (e) {
                e.preventDefault();
                if ($(this).attr('aria-expanded') != 'true') {
                    dropdown_close();
                    $(this).attr('aria-expanded', 'true');
                    $(this).siblings('.dropdown').stop().slideDown(300);
                } else {
                    $(this).attr('aria-expanded', 'false');
                    $(this).siblings('.dropdown').stop().slideUp(300);
                }
            });

            $(document).on('click touchend', function (e) {
                if (!$(e.target).is('.dropdown-nav-section .dropdown-wrap > a')) {
                    dropdown_close();
                }
            });
        }
    }



    // 패럴랙스 초기화
    function parallax_init() {
        $('[data-parallax]').parallax();
    }
});