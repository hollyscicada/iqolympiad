/**
 * AJAX 공통소스
 */
function ajaxCallPost(url, param, callbackSuccess, callbackFail){
	console.log("=========================================================")
	console.log("endPoint : " + url);
	console.log(param);
	$.ajax({
		type : "POST",
		url : url,
		contentType : "application/json",
		dataType : "JSON",    //옵션이므로 JSON으로 받을게 아니면 안써도 됨
		data : JSON.stringify(param),
		success : function(res) {
			console.log(res);
    		console.log("=========================================================")
    		if(res.errorMsg){
				alert(res.errorMsg)
			}
			callbackSuccess(res);
		},
		error : function(xhr, status, error) {
			console.log(error);
    		console.log("=========================================================")
    		alert("You have encountered an error. Please contact our development team.")
			callbackFail(error);
		}
	});
}


function ajaxCallGet(url, callbackSuccess, callbackFail){
	console.log("=========================================================")
	console.log("endPoint : " + url);
	$.ajax({
		type : "GET",
		url : url,
		contentType : "application/json",
		success : function(res) {
			console.log(res);
    		console.log("=========================================================")
    		if(res.errorMsg){
				alert(res.errorMsg)
			}
			callbackSuccess(res);
		},
		error : function(xhr, status, error) {
			console.log(error);
    		console.log("=========================================================")
    		alert("You have encountered an error. Please contact our development team.")
			callbackFail(error);
		}
	});
}
function ajaxCallGetToken(url, authToken, callbackSuccess, callbackFail){
	console.log("=========================================================")
	console.log("endPoint : " + url);
	console.log("authToken : " + authToken);
	$.ajax({
		type : "GET",
		url : url,
		contentType : "application/json",
		beforeSend : function(header){ 
			header.setRequestHeader("authToken", authToken);
		},
		success : function(res) {
			console.log(res);
			console.log("=========================================================")
			if(res.errorMsg){
				alert(res.errorMsg)
			}
			callbackSuccess(res);
		},
		error : function(xhr, status, error) {
			console.log(error);
			console.log("=========================================================")
			alert("You have encountered an error. Please contact our development team.")
			callbackFail(error);
		}
	});
}
function ajaxCallPostToken(url, authToken, param, callbackSuccess, callbackFail){
	console.log("=========================================================")
	console.log("endPoint : " + url);
	console.log("authToken : " + authToken);
	console.log(param);
	$.ajax({
		type : "POST",
		url : url,
		contentType : "application/json",
		dataType : "JSON",    //옵션이므로 JSON으로 받을게 아니면 안써도 됨
		data : JSON.stringify(param),
		beforeSend : function(header){ 
			header.setRequestHeader("authToken", authToken);
		},
		success : function(res) {
			console.log(res);
    		console.log("=========================================================")
    		if(res.errorMsg){
    			alert(res.errorMsg)
    		}
			callbackSuccess(res);
		},
		error : function(xhr, status, error) {
			console.log(error);
    		console.log("=========================================================")
    		alert("You have encountered an error. Please contact our development team.")
			callbackFail(error);
		}
	});
}
function ajaxUpload(inputFile, subPash, callback){
	var formData = new FormData();
	formData.append("file", inputFile);
	formData.append("sub_path", subPash);
	$.ajax({
		url : "/api/file/ajaxupload"
			, type : "POST"
				, processData : false
				, contentType : false
				, data : formData
				, success:function(res) {
					callback(res)
				},
				error : function(xhr, status, error) {
				}
	});
}