$(function(){
	$(document).on("click", ".comingsoon-btn", function(){
		alert("This feature is coming soon. Please come back later.")
	})
})
//0~9까지의 난수
function random(n1, n2) {
  return parseInt(Math.random() * (n2 -n1 +1)) + n1;
};
//인증번호를 뽑을 난수 입력 n 5이면 5자리
function randomNumber(n) {
  var value = "";
  for(var i=0; i<n; i++){
      value += random(0,9);
  }
  return value;
};


var global = {
		comma:function(str) {
		    str = String(str);
		    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
		},
		uncomma:function(str) {
			str = String(str); 
			return str.replace(/[^\d]+/g, '');
		},
		hipn:function(str) {
			return str.replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-");
		},
		chkPw:function(pw){
			var pattern1 = /[0-9]/;
	        var pattern2 = /[a-zA-Z]/;
//	        var pattern3 = /[~!@\#$%<>^&*]/;     
	        if(!pattern1.test(pw)||!pattern2.test(pw)||pw.length<8||pw.length>24){
	            return false;
	        }else{
	        	return true;
	        }          
		},
		getDevice:function(){
			var filter = "win16|win32|win64|macintel|mac|"; // PC일 경우 가능한 값
			if( navigator.platform) {
			     if( filter.indexOf(navigator.platform.toLowerCase())<0 ) {
			           return "M";
			     } else {
			           return "P"
			     }
			}
		}
}