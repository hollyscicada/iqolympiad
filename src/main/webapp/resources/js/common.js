var is_show = false;
$(document).on("click", ".showYnBtn", function(e){
	is_show = true;
	$(this).find("input").attr("checked",true);
	var table_name = $(this).attr("data-table-name");
	var table_seq_name = $(this).attr("data-table-seq");
	
	var seq = $(this).attr("data-seq");
	var value = $(this).attr("data-role");
	$.ajax({
		type:"GET",  
		url:"/supervise/api/v1/show/"+table_name+"/"+table_seq_name+"/"+seq+"/"+value,
		success:function(args) {
			if(args != "Y"){
				alert("노출여부 데이터 수정작업에 실패하였습니다.");
			}
			is_show = false;
		},
		error: function(request, status, error){
			console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
})