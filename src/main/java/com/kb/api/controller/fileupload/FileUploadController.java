package com.kb.api.controller.fileupload;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kb.api.service.MasterService;


/**
 * @version : java1.8 
 * @author : ohs
 * @date : 2018. 1. 17.
 * @msg : 파일 업로드 하는 함수들 바이너리형식으로 요청, multipart-form-data 형식  두가지 지원가능
*/
@Controller
@RequestMapping("/api/file/*")
public class FileUploadController {
	String setCharEncoding = "UTF-8";
	
	@Inject MasterService masterService; 
	
	/**
	 * @msg : 업로드 관련 path 모음
	 */
	public static String UPLOAD_PATH = "/resources/images/";
	
	
	/**
	 * @msg : 파일업로드 하는 객체이며 multipart 형식으로 넘어왔을 때만 사용하는 객체이며 thumb를 만든다. 또한 위의 객체와 리턴방식이 다름
	 */
	@PostMapping(value = "/ajaxupload") // thumb 도 만들기
	private @ResponseBody HashMap<String, Object> ajaxupload(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) throws Exception {
		response.setContentType("text/html; charset="+setCharEncoding+" \" ");
		String subPath = request.getParameter("sub_path");
		subPath = UPLOAD_PATH+subPath+"/";
		
		String realPath = request.getServletContext().getRealPath(subPath);
		HashMap<String, Object> rstMap = multipart_upload(request, realPath, subPath);
		
		return rstMap;
	}
	
	/**
	 * @message : 실질적인 업로드하는 객체
	 * @param request : 업로드할 객체를 가지고 있는 요청데이터
	 * @param realPath : 실질적으로 사진을 업로드할 경고
	 * @return : 이미지 업로드 후 업로드한 파일명
	 */
	public HashMap<String, Object> multipart_upload(HttpServletRequest request, String realPath, String webUrl) throws IllegalStateException, IOException{
		mkDir(realPath);
		String filename = "";
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		MultipartFile mrequest = null;
		
		while(iterator.hasNext()){
			mrequest = multipartHttpServletRequest.getFile(iterator.next());
			
			String filenameTemp = mrequest.getOriginalFilename();
			String ext = filenameTemp.substring(filenameTemp.lastIndexOf(".") + 1); //확장자 구하기
			filename += UUID.randomUUID().toString().replace("-", "")+"."+ext;
			if(mrequest.isEmpty() == false) {
				mrequest.transferTo(new File(realPath + filename));
				
				/**
				 * 디비에 저장
				 */
				HashMap paramMap = new HashMap();
				paramMap.put("ori_name", filenameTemp);
				paramMap.put("ext", ext);
				paramMap.put("name", filename);
				paramMap.put("url", webUrl+filename);
				
				int record = masterService.dataCreate("mapper.FileMapper", "file_add", paramMap);
				
				if(record > 0) {
					return paramMap;
				}
				
			}
		}
		return null;
	}
	
	/**
	 * @param realPath : 해당 경로에 디렉토리가 있다면 넘어가고 없다면 디렉토리를 생성한다.
	 */
	public static void mkDir(String realPath){
		File updir = new File(realPath);
		if (!updir.exists()) updir.mkdirs();
	}
}
