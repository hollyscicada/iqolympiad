package com.kb.api.controller.member;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.Util;

@RestController 
public class MemberTestRestController {   
	
	@Inject MasterService masterService; 
	 
	@PostMapping("/api/v1/member/iq/test")    
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		String[] valids = {"page"};  
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);  
		if(validEntity != null) return validEntity;  
		   
		ResponseEntity<Map<String, Object>> entity = null;  
		Map<String, Object> map = new HashMap<String, Object>();  
		  
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block()); 
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block()); 
		param.put("page_block", param.getPage_block()); 
		
 
		HashMap paramMap = new HashMap(param); 
		
		int record = masterService.dataCount("mapper.MemberTestMapper", "test_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.MemberTestMapper", "test", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	@PostMapping("/api/v1/member/iq/new/test")   
	public ResponseEntity<Map<String, Object>> new_test(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		 
		
		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.MemberTestMapper", "new_test", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	@PostMapping("/api/v1/member/iq/category/test")   
	public ResponseEntity<Map<String, Object>> category_test(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		
		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.MemberTestMapper", "category_test", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
}
