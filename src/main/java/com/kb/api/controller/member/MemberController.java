package com.kb.api.controller.member;
 
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.kb.api.service.MasterService;
import com.kb.api.service.member.MemberService;
import com.kb.api.util.Util;

@Controller 
public class MemberController {   
	
	@Inject MasterService masterService; 
	@Inject MemberService memberService; 
	 
	
	@GetMapping("/member/mypage") 
	public String mypage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String str = memberService.commonChkSession(model, session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		return "layout/member/myAccount"; 
	}
	
	@GetMapping("/member/mypage/edit") 
	public String edit(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String str = memberService.commonChkSession(model, session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		return "layout/member/myAccount_edit";
	}
	
	@GetMapping("/terms") 
	public String terms(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/member/terms";
	}
	
}
