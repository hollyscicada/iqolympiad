package com.kb.api.controller.login;
 
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.service.login.LoginService;
import com.kb.api.service.ranking.RankingService;
import com.kb.api.util.AES256Util;
import com.kb.api.util.Common;
import com.kb.api.util.MailSendUtil;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.Util;

@RestController 
public class LoginRestController {
	 
	@Inject MasterService masterService; 
	@Inject MailSendUtil mailSendUtil;
	@Inject LoginService loginService;   
	
	
	/** 
	 * 로그인 
	 */
	@PostMapping("/api/v1/login")   
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;  
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		
		String[] valids = {"login_type", "login_code"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "login", paramMap);
		
		
		
		if (retMap != null) {
			paramMap.put("member_seq", retMap.get("member_seq"));
			loginService.commonMakeSession(retMap, map, paramMap, session);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/** 
	 * 자동로그인 
	 */
	@PostMapping("/api/v1/auto/login")  
	public ResponseEntity<Map<String, Object>> auto_login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		String[] valids = {"custom_type", "custom_code"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "auto_login", paramMap);
     		
 
		 
		if (retMap != null) {
			paramMap.put("member_seq", retMap.get("member_seq"));
			loginService.commonMakeSession(retMap, map, paramMap, session);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 공통 중복체크 ( 메일, 아이디)
	 */
	@PostMapping("/api/v1/common/chk")  
	public ResponseEntity<Map<String, Object>> common_chk(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "common_chk", paramMap);
		
		if(retMap != null) {
			map.put("success", true);//중복되는게 잇을경우 true
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 회원가입
	 */
	@PostMapping("/api/v1/sign")  
	public ResponseEntity<Map<String, Object>> join(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		String[] valids = {"login_type", "login_code", "email", "name", "sex", "age", "country_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		

		HashMap paramMap = new HashMap(param);
		
		map = loginService.sign(paramMap, map, session);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 사용자 정보
	 */
	@PostMapping("/api/v1/user/info/seq")  
	public ResponseEntity<Map<String, Object>> user_info(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "user_info_seq", paramMap); 
		map.put("data", retMap);
 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/**
	 * 내정보수정
	 */
	@PostMapping("/api/v1/change/member")  
	public ResponseEntity<Map<String, Object>> change_info(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false); 

		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = null;
		int record = masterService.dataUpdate("mapper.LoginMapper", "change_info", paramMap);
		
		if(record > 0) {
			loginService.commonMakeSession(retMap, map, paramMap, session);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	
	/**
	 * 회원탈퇴  
	 */ 
	@PostMapping("/api/v1/member/delete")  
	public ResponseEntity<Map<String, Object>> member_delete(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
 
		HashMap paramMap = new HashMap(param); 
		int record = masterService.dataUpdate("mapper.LoginMapper", "member_delete", paramMap);
		
		if(record > 0) {
			session.invalidate();
			map.put("success", true); 
		} 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	
	
}
