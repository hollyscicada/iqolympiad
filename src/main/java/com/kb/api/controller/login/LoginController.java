package com.kb.api.controller.login;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.api.service.MasterService;
import com.kb.api.service.login.LoginService;
import com.kb.api.service.main.MainService;
import com.kb.api.util.Common;

@Controller
public class LoginController {  
	
	 
	@Inject MainService mainService;  
	@Inject MasterService masterService;  
	@Inject LoginService loginService;  
	
	@Value("#{props['facebook.key']}") private String facebook_key;
	@Value("#{props['facebook.pw']}") private String facebook_pw;
	@Value("#{props['facebook.redirect.url']}") private String facebook_url;
	
	@Value("#{props['google.key']}") private String google_key;
	@Value("#{props['google.pw']}") private String google_pw;
	@Value("#{props['google.redirect.url']}") private String google_url;
	
	
	@GetMapping("/login") 
	public String login(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/login/login";
	}
	@GetMapping("/sign") 
	public String sign(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/login/sign";
	}
	@GetMapping("/sign/success") 
	public String sign_success(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/login/signResult";
	}
	@RequestMapping(value = {"/logout"}, method =  { RequestMethod.GET , RequestMethod.POST })
	public String logout(Model model, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		session.invalidate();
		return "common/logout";
	}
	
	
	
	/**
	 * 페이스북 리절트
	 */
	@GetMapping("/facebook/oauth") 
	public String facebook_oauth(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String code = request.getParameter("code");
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("client_id", facebook_key);
		map.put("redirect_uri", facebook_url);     
		map.put("client_secret", facebook_pw);
		map.put("code", code);
		
		String rst = Common.sendPush("https://graph.facebook.com/v9.0/oauth/access_token", "GET", null, map);
		
		JSONParser parser = new JSONParser();
		JSONObject jObject = (JSONObject) parser.parse(rst);
		String access_token = jObject.get("access_token").toString();
		
		

		
		rst = Common.sendPush("https://graph.facebook.com/me?fields=id,name,email&access_token="+access_token, "GET", null, null);
		jObject = (JSONObject) parser.parse(rst);
		
		 System.out.println("리턴데이터 " + jObject.toString());
		
		String id = jObject.get("id").toString();
		System.out.println("페이스북 아이디 값은 : " + id);
		return loginService.login(id, "F", session);
	}
	
	
	/**
	 * 구글 리절트
	 */
	@GetMapping("/google/oauth") 
	public String google_oauth(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String code = request.getParameter("code");
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("grant_type", "authorization_code");
		map.put("client_id", google_key);
		map.put("redirect_uri", google_url);
		map.put("client_secret", google_pw);
		map.put("code", code);
		
		String rst = Common.sendPush("https://oauth2.googleapis.com/token", "POST", null, map);
		JSONParser parser = new JSONParser();
		JSONObject jObject = (JSONObject) parser.parse(rst);
		String access_token = jObject.get("access_token").toString();
		
		rst = Common.sendPush("https://www.googleapis.com/oauth2/v3/userinfo?alt=json&access_token="+access_token, "GET", null, null);
		jObject = (JSONObject) parser.parse(rst);
		
		 System.out.println("리턴데이터 " + jObject.toString());
		
		
		String id = jObject.get("sub").toString();
		System.out.println("구글 아이디 값은 : " + id);
		
		return loginService.login(id, "G", session);
	}
}
