package com.kb.api.controller.main;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.kb.api.service.MasterService;
import com.kb.api.service.main.MainService; 

@Controller
public class MainController {  
	 
	@Inject MainService mainService;  
	@Inject MasterService masterService;  
	@GetMapping("") 
	public String main(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/main";
	}
}
