package com.kb.api.controller.iq;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.service.iq.IqTestResultService;
import com.kb.api.service.iq.IqTestService;
import com.kb.api.util.Common;
import com.kb.api.util.ErrorCode;
import com.kb.api.util.ProHashMap;
  
@RestController 
public class IqTestRestController {   
	
	@Inject MasterService masterService; 
	@Inject IqTestService iqTestService;   
	@Inject IqTestResultService iqTestResultService;   
	   
	    
	@PostMapping("/api/v1/iq/test")   
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		HashMap paramMap = new HashMap(param);  
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.IqTestMapper", "test", paramMap);
		map.put("data", retMap);
		
		 
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	 
	@PostMapping("/api/v1/iq/test/{test_seq}")   
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param, @PathVariable String test_seq) throws Exception {
		param.put("test_seq", test_seq);
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		HashMap paramMap = new HashMap(param);
		try {  
			HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.IqTestMapper", "test_read", paramMap);
			
			List<HashMap<String, Object>> rankingList = new ArrayList<HashMap<String,Object>>();
			if(!StringUtils.isEmpty(retMap) && !StringUtils.isEmpty(retMap.get("write_type")) && retMap.get("write_type").toString().equals("M")) {
					retMap.put("ranking_list", iqTestResultService.makeScoreScope(retMap));
			}
			  
			List<HashMap<String, Object>> recommendList = new ArrayList<HashMap<String,Object>>();
			if(!StringUtils.isEmpty(retMap)) {
				recommendList = (List) masterService.dataList("mapper.IqTestMapper", "recommend", retMap);
				retMap.put("recommend_list", recommendList);
			}
			
			List<HashMap<String, Object>> historyList = new ArrayList<HashMap<String,Object>>();
			if(!StringUtils.isEmpty(retMap)) {
				historyList = (List) masterService.dataList("mapper.IqTestMapper", "history", retMap);
				retMap.put("history_list", historyList);
			} 
			
			List<HashMap<String, Object>> bestRankingList = new ArrayList<HashMap<String,Object>>();
			if(!StringUtils.isEmpty(retMap)) {
				bestRankingList = (List) masterService.dataList("mapper.IqTestMapper", "iqRanking123", paramMap);
				retMap.put("best_ranking_list", bestRankingList);
			}
			
			int one = masterService.dataCount("mapper.IqTestMapper", "iqTestIngCntOne", paramMap);
			int two = masterService.dataCount("mapper.IqTestMapper", "iqTestIngCntTwo", paramMap);
			
			retMap.put("one", one);
			retMap.put("two", two);
			
			
			map.put("data", retMap);
		}catch(Exception e) {
			ErrorCode.makeError(map, e);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	 
	@PostMapping("/api/v1/iq/test/{test_seq}/cnt")   
	public ResponseEntity<Map<String, Object>> test_cnt(HttpSession session, @RequestBody ProHashMap param, @PathVariable String test_seq, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		param.put("test_seq", test_seq);
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);
		int record = masterService.dataCount("mapper.IqTestMapper", "test_cnt", paramMap);
		
		map.put("success", true);
		if(record >= 2) {
			map.put("success", false);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	@PostMapping("/api/v1/iq/test/{test_seq}/cnt/one")   
	public ResponseEntity<Map<String, Object>> test_cnt_one(HttpSession session, @RequestBody ProHashMap param, @PathVariable String test_seq, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		param.put("test_seq", test_seq);
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		 
		HashMap paramMap = new HashMap(param);
		int record = masterService.dataCount("mapper.IqTestMapper", "test_cnt", paramMap);
		
		map.put("success", false);//hide
		if(record == 0) {
			map.put("success", true);//show
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/iq/result/test/add")   
	public ResponseEntity<Map<String, Object>> result_test_add(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {

		String member_seq = "";
		if(!StringUtils.isEmpty(request.getHeader("authToken"))) {
			member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq); 
		}
		 
		String[] valids = {"test_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false); 
		
		HashMap paramMap = new HashMap(param);
		try {
			map = iqTestService.result_test_add(paramMap, map, session);
		}catch(Exception e) {
			map.put("errorMsg", "테스트 진행도중 에러가 발생하였습니다.");
			if(e.getMessage().equals("NOTPOINT")) {
				map.put("errorMsg", "잔액이 부족합니다.");
			}
			if(e.getMessage().equals("ERROR_LOGIN")) {
				map.put("errorMsg", "UCT 테스트를 진행하려면 로그인이 필요합니다.");
				map.put("errorCode", "599"); //로그인페이지로 이동시키기
			}
			
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity)); 
		return entity; 
	}
	
	@PostMapping("/api/v1/iq/test/ing")   
	public ResponseEntity<Map<String, Object>> iqTestIng(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = ""; 
		if(!StringUtils.isEmpty(request.getHeader("authToken"))) {
			member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		
		String[] valids = {"result_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity; 
		
		
		
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.IqTestMapper", "iqTestIng", paramMap);
		
		List<HashMap<String, Object>> questionList = new ArrayList<HashMap<String,Object>>();
		if(!StringUtils.isEmpty(retMap)) {
			retMap.put("result_seq", paramMap.get("result_seq")); 
			questionList = (List) masterService.dataList("mapper.IqQuestionMapper", "questionIng", retMap);
			retMap.put("question_list", questionList);
		}
		if(!StringUtils.isEmpty(questionList)) {
			for(int i = 0 ; i < questionList.size() ; i++) {
				questionList.get(i).put("result_seq", paramMap.get("result_seq"));
				List<HashMap<String, Object>> exampleList = (List) masterService.dataList("mapper.IqExampleMapper", "exampleIng", questionList.get(i));
				questionList.get(i).put("example_list", exampleList);
			}
		}
		
		
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK); 
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	} 
	
	@PostMapping("/api/v1/iq/test/poling")   
	public ResponseEntity<Map<String, Object>> iqTestPoling(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		
		String[] valids = {"result_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity; 
		
		
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);  
		int record = masterService.dataCount("mapper.IqTestMapper", "iqTestPoling", paramMap);
		
		HashMap<String, Object> retMap = new HashMap<String, Object>(); 
		retMap.put("record", record);
		
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
