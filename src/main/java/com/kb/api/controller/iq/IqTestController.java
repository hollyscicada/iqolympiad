package com.kb.api.controller.iq;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.kb.api.service.member.MemberService;
import com.kb.api.util.Util;

@Controller
public class IqTestController {  
	
	@Inject MemberService memberService;  
	
	@GetMapping("/TestsMain") 
	public String TestsMain(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/test/TestsMain";
	}
	@GetMapping("/TestsDetail") 
	public String TestsDetail(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/test/TestsDetail";
	}
	@GetMapping("/TestsResult") 
	public String TestsResult(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/test/TestsResult";
	}
	@GetMapping("/TestsResultEvent") 
	public String TestsResultEvent(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "layout/test/TestsResultEvent";
	}
	@GetMapping("/dashboardMain") 
	public String dashboardMain(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception { 
		
		String str = memberService.commonChkSession(model, session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		return "layout/dashboard/dashboardMain";
	}
	@GetMapping("/dashboardDetail") 
	public String dashboardDetail(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		String str = memberService.commonChkSession(model, session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		return "layout/dashboard/dashboardDetails";
	}
	@GetMapping("/dashboardCertification") 
	public String sign(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		String str = memberService.commonChkSession(model, session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		return "layout/dashboard/dashboardCertification";
	}
}
