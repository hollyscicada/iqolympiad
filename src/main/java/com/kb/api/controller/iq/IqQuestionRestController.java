package com.kb.api.controller.iq;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.Util;

@RestController 
public class IqQuestionRestController {   
	
	@Inject MasterService masterService; 
	 
	@PostMapping("/api/v1/iq/question")   
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"test_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		 

		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.IqQuestionMapper", "question", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/iq/question/{question_seq}")   
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param, @PathVariable String question_seq) throws Exception {
		param.put("question_seq", question_seq);
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.IqQuestionMapper", "question_read", paramMap);
		
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String,Object>>();
		if(!StringUtils.isEmpty(retMap)) {
			retList = (List) masterService.dataList("mapper.IqExampleMapper", "example", retMap);
			retMap.put("example_list", retList);
		}
		
		map.put("data", retMap);
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
}
