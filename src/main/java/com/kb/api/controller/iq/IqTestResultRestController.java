package com.kb.api.controller.iq;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.service.iq.IqTestResultService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class IqTestResultRestController {   
	
	@Inject MasterService masterService; 
	@Inject IqTestResultService iqTestResultService; 
	  
	/** 
	 * 테스트 종료, 임시저장
	 * @param session
	 * @param param  
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/api/v1/iq/test/end")   
	public ResponseEntity<Map<String, Object>> testEnd(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = "";
		if(!StringUtils.isEmpty(request.getHeader("authToken"))) {
			member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		
		String[] valids = {"result_seq", "example_seqs", "type"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		 
		HashMap paramMap = new HashMap(param);
		map.put("success", false); 
		
		
		try {
			map = iqTestResultService.testEnd(paramMap, map); 
		}catch(Exception e) {
			e.printStackTrace(); 
			map.put("errorMsg", "IQ 테스트 결과제출에 실패하였습니다."); 
			if(e.getMessage().equals("CHKENDTIME")) {
				map.put("errorMsg", "이전에 IQ 테스트 결과제출을 진행한 데이터입니다.");
			}
			if(e.getMessage().equals("ERROR_EXAMPLE")) {
				map.put("errorMsg", "example_seqs 요청 데이터를 다시확인해주세요.");
			} 
			if(e.getMessage().equals("ERROR_TYPE")) {
				map.put("errorMsg", "type는 'TEMPSAVE', 'END' 두가지 데이터만 요청할수 있습니다.");
			}
			if(e.getMessage().equals("ERROR_NOT_LOGIN_END")) {
				map.put("errorMsg", "로그인 하지 않을경우 임시저장만 가능합니다."); 
			}
			if(e.getMessage().equals("ERROR_SCORE")) {
				map.put("errorMsg", "점수 산정에서 에러가 발생하였습니다.");
			}
			if(e.getMessage().equals("EVENT_TYPE_ERROR")) {
				map.put("errorMsg", "테스트의 이벤트 타입여부가 존재하지 않습니다.");
			}
			if(e.getMessage().equals("EVENT_SCORE_ERROR")) {
				map.put("errorMsg", "테스트의 이벤트 점수산정에서 에러가 발생하였습니다.");
			}
			
		}
		
		 
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 임시저장(사용자정보 업데이트)
	 * @param session
	 * @param param
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/api/v1/iq/test/save/update")   
	public ResponseEntity<Map<String, Object>> saveUpdate(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		String[] valids = {"result_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		 
		HashMap paramMap = new HashMap(param);
		map.put("success", false);
		map.put("redirectType", "NORMAL");
		
		
		try {
			map = iqTestResultService.saveUpdate(paramMap, map);
		}catch(Exception e) {
			map.put("errorMsg", "IQ 테스트 결과제출에 실패하였습니다.");
			if(e.getMessage().equals("ERROR_CHK")) {
				map.put("errorMsg", "해당 테스트가 존재하지 않습니다.");
			}
			if(e.getMessage().equals("READ_CHK")) {
				map.put("errorMsg", "해당 테스트가 존재하지 않습니다.");
			}
			if(e.getMessage().equals("SCORE_UPDATE_ERROR")) {
				map.put("errorMsg", "해당 테스트의 점수를 나의 IQ 점수 반영중 에러가 발생하였습니다.");
			}
			if(e.getMessage().equals("ERROR_CHK_TEST_CNT")) {
				map.put("errorCode", "505");
				map.put("errorMsg", "테스트는 최대 2회까지만 진행 할수 있습니다.");
			}
		}
		
		 
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**  
	 * 테스트 결과   
	 */  
	@PostMapping("/api/v1/iq/test/result")    
	public ResponseEntity<Map<String, Object>> testResult(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		String[] valids = {"result_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;   
		
		 
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.IqTestResultMapper", "result", paramMap);
		map.put("data", retMap);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**  
	 * 테스트 결과   
	 */  
	@PostMapping("/api/v1/iq/test/result/event")    
	public ResponseEntity<Map<String, Object>> testResultEvent(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String[] valids = {"result_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;   
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();   
		
		HashMap paramMap = new HashMap(param);
		 
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.IqTestResultMapper", "resultEvent", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**  
	 * 이벤트 결과내용   
	 */  
	@PostMapping("/api/v1/iq/event/result/resources")    
	public ResponseEntity<Map<String, Object>> eventResult(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		
		String[] valids = {"score"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;   
		 
		 
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> bestMap = (HashMap<String, Object>) masterService.dataRead("mapper.IqTestResultMapper", "eventResultListBest", paramMap);
		List<HashMap<String, Object>> jobList = (List<HashMap<String, Object>>) masterService.dataList("mapper.IqTestResultMapper", "eventResultListJob", paramMap);
		
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("bestMap", bestMap);
		rstMap.put("jobList", jobList);
		
		map.put("data", rstMap);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/iq/event/result/resources/upd")    
	public ResponseEntity<Map<String, Object>> eventResultUpd(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		
		String[] valids = {"result_seq", "event_seq"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;   
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		HashMap paramMap = new HashMap(param);
		
		map.put("success", false);
		
		int record = masterService.dataUpdate("mapper.IqTestResultMapper", "eventResultListBestUpd", paramMap);
		if(record>0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	 
}
