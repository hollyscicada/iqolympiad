package com.kb.api.controller.common;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.Util;

@RestController 
public class CommonRestController {   
	
	@Inject MasterService masterService; 
	 
	    
	/**
	 * 공통코드
	 */
	@PostMapping("/api/v1/common")   
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		String[] valids = {"up_code"};
		ResponseEntity<Map<String, Object>> validEntity = Common.validation(valids, param);
		if(validEntity != null) return validEntity;
		 

		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.CommonMapper", "code_list", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
}
