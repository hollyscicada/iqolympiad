package com.kb.api.job;

import javax.inject.Inject;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.kb.api.service.MasterService;

@Component
public class RankingJob {  
	
	@Inject MasterService masterService;
	
	/**
	 * 매월 1일 0시 1분 
	 */
	@Scheduled(cron="0 1 0 * * *")
	public void memberHistoryCreate(){
		int recrod = masterService.dataCreate("mapper.RankingMapper", "member_histroy_add", null);
	}
}
