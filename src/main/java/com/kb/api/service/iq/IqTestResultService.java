package com.kb.api.service.iq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.kb.api.persistence.MasterDao;
@Service
public class IqTestResultService {
	@Inject MasterDao MasterDao;
	private static final Logger logger = LoggerFactory.getLogger(IqTestResultService.class);
	 
	@Transactional(value="transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> testEnd(HashMap<String, Object> paramMap, Map<String, Object> map)  throws Exception{
		
		/**
		 * 타입 유효성체크
		 */
		if(!paramMap.get("type").equals("TEMPSAVE") && !paramMap.get("type").equals("END")){
			throw new Exception("ERROR_TYPE");
		}
		
		/**
		 * 이전에 제출했었던 이력이 있는지
		 */
		int chk = MasterDao.dataCountTra("mapper.IqTestResultMapper", "rankingCnt", paramMap);
		if(chk > 0) {
			throw new Exception("CHKENDTIME");
		}
		
		/**
		 * 타입 가져오기
		 */
		HashMap<String, Object> testMap = null;
		testMap = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.IqTestResultMapper", "resultTestInfo", paramMap);
		
		if(StringUtils.isEmpty(testMap)) {
			throw new Exception();
		}
		String event_yn = testMap.get("event_yn").toString();
		
		
		MasterDao.dataDeleteTra("mapper.IqTestResultMapper", "end_test_example_del", paramMap); 
		 
		int record = MasterDao.dataCreateTra("mapper.IqTestResultMapper", "end_test_example", paramMap);
		if(record > 0) { 
			if(paramMap.get("type").equals("END")) {
				
				
				
			 
				/**
				 * 점수 계산하기
				 */
				HashMap<String, Object> scoreMap = null;
				scoreMap = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.IqTestResultMapper", "makeScore", paramMap);
				
				if(scoreMap == null) {
					throw new Exception("ERROR_SCORE");
				}else {
					
					String write_type = testMap.get("write_type").toString();
					 
					/*BCT의 경우의 계산법*/
					if(!StringUtils.isEmpty(write_type) && write_type.equals("A")) {
						int cnt = Integer.parseInt(scoreMap.get("cnt").toString());
						 
						 
						if(!StringUtils.isEmpty(event_yn) && event_yn.equals("Y")) {
							/*이벤트 bct 점수 채점*/ 
							if(cnt == 0) scoreMap.put("score", "80");    
							else if(cnt == 1) scoreMap.put("score", "85"); 
							else if(cnt == 2) scoreMap.put("score", "90");
							else if(cnt == 3) scoreMap.put("score", "95");
							else if(cnt == 4) scoreMap.put("score", "100");
							else if(cnt == 5) scoreMap.put("score", "110");
							else if(cnt == 6) scoreMap.put("score", "120");
							else if(cnt == 7) scoreMap.put("score", "130");
							else if(cnt == 8) scoreMap.put("score", "140"); 
							else if(cnt == 9) scoreMap.put("score", "150");
							else if(cnt == 10) scoreMap.put("score", "160"); 
							else throw new Exception("EVENT_SCORE_ERROR");
							
							
							
							
						}else if(!StringUtils.isEmpty(event_yn) && event_yn.equals("N")) {
							
							/*일반 bct 점수 채점*/
							if(cnt >= 38) {
								scoreMap.put("score", "160");
							}else if(cnt < 8) {
								scoreMap.put("score", "40");
							}else {
								scoreMap.put("score", Integer.parseInt(scoreMap.get("score").toString()) + 8);
							}
							
							
							/*일반 bct 퍼센트 저장하기*/
							String persent = "0";
							if(cnt == 0) persent = "0";
							else if(cnt == 1) persent = "0";
							else if(cnt == 2) persent = "0";
							else if(cnt == 3) persent = "0";
							else if(cnt == 4) persent = "0.003";
							else if(cnt == 5) persent = "0.003";
							else if(cnt == 6) persent = "0.003";
							else if(cnt == 7) persent = "0.003";
							else if(cnt == 8) persent = "0.003";
							else if(cnt == 9) persent = "0.010";
							else if(cnt == 10) persent = "0.020";
							else if(cnt == 11) persent = "0.060";
							else if(cnt == 12) persent = "0.160";
							else if(cnt == 13) persent = "0.300";
							else if(cnt == 14) persent = "0.800";
							else if(cnt == 15) persent = "2.200";
							else if(cnt == 16) persent = "3.000";
							else if(cnt == 17) persent = "5.000";
							else if(cnt == 18) persent = "9.000";
							else if(cnt == 19) persent = "14.000";
							else if(cnt == 20) persent = "21.000";
							else if(cnt == 21) persent = "29.000";
							else if(cnt == 22) persent = "39.000";
							else if(cnt == 23) persent = "50.000";
							else if(cnt == 24) persent = "61.000";
							else if(cnt == 25) persent = "71.000";
							else if(cnt == 26) persent = "79.000";
							else if(cnt == 27) persent = "86.000";
							else if(cnt == 28) persent = "91.000";
							else if(cnt == 29) persent = "95.000";
							else if(cnt == 30) persent = "97.000";
							else if(cnt == 31) persent = "97.800";
							else if(cnt == 32) persent = "99.200";
							else if(cnt == 33) persent = "99.700";
							else if(cnt == 34) persent = "99.840"; 
							else if(cnt == 35) persent = "99.940";
							else if(cnt == 36) persent = "99.980"; 
							else if(cnt == 37) persent = "99.990";
							else if(cnt == 38) persent = "99.997";
							else if(cnt == 39) persent = "99.997";
							else if(cnt == 40) persent = "99.997";
							
							HashMap<String, Object> persentMap = new HashMap<String, Object>();
							persentMap.put("result_seq", testMap.get("result_seq"));
							persentMap.put("persent", persent);
							record += MasterDao.dataUpdateTra("mapper.IqTestResultMapper", "updatePersent", persentMap);
							
							
						}else {
							throw new Exception("EVENT_TYPE_ERROR");
						}
						
						
					}else if(!StringUtils.isEmpty(write_type) && write_type.equals("M")) {
						/*UCT의 경우의 계산법*/
						/**
						 * 나의등수 구하기
						 */
						
						/*프리미엄 구하기*/
						HashMap<String, Object> basicAve = null;
						basicAve = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.IqTestResultMapper", "basicTestAve", testMap);
						
						int ave = Integer.parseInt(basicAve.get("ave").toString());
						System.out.println("베이직테스트 평균점수 : " + ave);
						
						int premium = ave-100;
						System.out.println("premium : " + premium);
						
						
						/*퍼센티지 구하기*/
						int cnt = Integer.parseInt(scoreMap.get("cnt").toString());
						
						System.out.println("맞은 갯수 : "+cnt);
						List<HashMap<String, Object>> oneTestRankingList = (List<HashMap<String, Object>>) MasterDao.dataListTras("mapper.IqTestResultMapper", "oneTestRanking", testMap);
						double rank = 0;
						for(HashMap<String, Object> oneTestRanking : oneTestRankingList) {
							
							int testScope = Integer.parseInt(oneTestRanking.get("cnt").toString());
							if(cnt < testScope) {
								rank++;
							}
						}
						
						/*나보다 높은사람수 + 나포함*/
						rank++;
						
						double showTestCnt = oneTestRankingList.size();
						showTestCnt++;

						System.out.println("나보다 높은사람수(나포함) : "+rank);
						System.out.println("시험본사람(나포함+1) : "+showTestCnt);
						
						
						double percentage = 100-(rank/showTestCnt)*100; //14.55
						System.out.println("percentage : "+percentage);
						
						
						/*z core 구하기*/
						double zScore = this.makeZcore(percentage/100);
						System.out.println("zScore : "+zScore);
						
						
						/*최종 IQ 구하기*/
						double iq = Math.round(100+zScore*15+premium);
						scoreMap.put("score", iq); 
					}
					
					
					
					
				}
				
				
				
				record += MasterDao.dataUpdateTra("mapper.IqTestResultMapper", "end_test", scoreMap);
				if(record > 1) {
					
					
					if(!StringUtils.isEmpty(paramMap.get("member_seq")) && event_yn.equals("N")){
						paramMap.put("score", scoreMap.get("score"));
						record += MasterDao.dataUpdateTra("mapper.IqTestResultMapper", "update_member_iq", paramMap);
						
						if(record >= 3) {
							map.put("success", true); 
						}
					}else {
						if(record >= 2) {
							map.put("success", true); 
						}
					}
				} 
				
				
				
			}else if(paramMap.get("type").equals("TEMPSAVE")){
				
				int tempUpdateRecord = MasterDao.dataUpdateTra("mapper.IqTestResultMapper", "tempYnUpdate", paramMap);
				if(tempUpdateRecord > 0) {
					map.put("success", true);
				}
			}else {
				throw new Exception("ERROR_TYPE");
			}
		}else {
			throw new Exception("ERROR_EXAMPLE");
		}
		
		return map;
	}
	
	
	@Transactional(value="transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> saveUpdate(HashMap<String, Object> paramMap, Map<String, Object> map)  throws Exception{
		
		
		int record = MasterDao.dataCountTra("mapper.IqTestResultMapper", "chkSave", paramMap);
		
		if(record > 0) {
			HashMap<String, Object> retMap = null;
			retMap = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.IqTestResultMapper", "resultRead", paramMap);
			
			if(retMap != null) {
				String event_yn = retMap.get("event_yn").toString();
				retMap.put("member_seq", paramMap.get("member_seq"));
				
				if(event_yn.equals("N")) {
					int recordTest = MasterDao.dataCountTra("mapper.IqTestMapper", "test_cnt", retMap);
					if(recordTest >= 2) {
						throw new Exception("ERROR_CHK_TEST_CNT");
					}
				}
				
				paramMap.put("event_yn", retMap.get("event_yn"));
				
				
				/*iq 업데이트*/
				int recordUpdate = MasterDao.dataUpdateTra("mapper.IqTestResultMapper", "saveUpdate", paramMap);
				if(recordUpdate > 0) {
					
					
					/*이벤트 테스트의 경우 점수 업데이트 로직 안태움*/
					String tempYn = retMap.get("temp_yn").toString();
					if(tempYn.equals("N") && event_yn.equals("N")) {
						paramMap.put("score", retMap.get("score"));
						int scoreUpdate = MasterDao.dataUpdateTra("mapper.IqTestResultMapper", "update_member_iq", paramMap);
						
						if(scoreUpdate>0) {
							map.put("success", true);
							map.put("redirectType", "RESULT");
							map.put("data", paramMap);
						}else {
							throw new Exception("SCORE_UPDATE_ERROR");
						}
					}else {
						map.put("success", true);
						map.put("data", paramMap);
					}
				}
				
				
			}else {
				throw new Exception("READ_CHK");
			}
		}else {
			throw new Exception("ERROR_CHK");
		}
		return map;
	}
	
	
	public double makeZcore(double p) {
		
	    double a1 = -39.6968302866538, a2 = 220.946098424521, a3 = -275.928510446969;
	    double a4 = 138.357751867269, a5 = -30.6647980661472, a6 = 2.50662827745924;
	    double b1 = -54.4760987982241, b2 = 161.585836858041, b3 = -155.698979859887;
	    double b4 = 66.8013118877197, b5 = -13.2806815528857, c1 = -7.78489400243029E-03;
	    double c2 = -0.322396458041136, c3 = -2.40075827716184, c4 = -2.54973253934373;
	    double c5 = 4.37466414146497, c6 = 2.93816398269878, d1 = 7.78469570904146E-03;
	    double d2 = 0.32246712907004, d3 = 2.445134137143, d4 = 3.75440866190742;
	    double p_low = 0.02425, p_high = 1 - p_low;
	    double q, r;
	    double retVal;
	    if ((p < 0) || (p > 1))
	    {
	    	System.out.println("NormSInv: Argument out of range.");
	        retVal = 0;
	    }
	    else if (p < p_low)
	    {
	        q = Math.sqrt(-2 * Math.log(p));
	        retVal = (((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) / ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
	    }
	    else if (p <= p_high)
	    {
	        q = p - 0.5;
	        r = q * q;
	        retVal = (((((a1 * r + a2) * r + a3) * r + a4) * r + a5) * r + a6) * q / (((((b1 * r + b2) * r + b3) * r + b4) * r + b5) * r + 1);
	    }
	    else
	    {
	        q = Math.sqrt(-2 * Math.log(1 - p));
	        retVal = -(((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) / ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
	    }
	    return retVal;
	}
	
	
	public List<HashMap<String, Object>> makeScoreScope(HashMap<String, Object> retMap) throws Exception{
		
		
		List<HashMap<String, Object>> resList = new ArrayList<HashMap<String,Object>>();
		
		/*해당테스트 평균가져오기*/
		HashMap<String, Object> basicAve = null;
		basicAve = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.IqTestResultMapper", "basicTestAve", retMap);
		
		int ave = Integer.parseInt(basicAve.get("ave").toString());
		System.out.println("베이직테스트 평균점수 : " + ave);
		
		int premium = ave-100;
		System.out.println("premium : " + premium);
		
		if(StringUtils.isEmpty(retMap.get("question_cnt"))) {
			throw new Exception("900");
		}
		
		List<HashMap<String, Object>> oneTestRankingList = (List<HashMap<String, Object>>) MasterDao.dataListTras("mapper.IqTestResultMapper", "oneTestRanking", retMap);
		
		
		int question_cnt = Integer.parseInt(retMap.get("question_cnt").toString());
		for(int i = 0 ; i < question_cnt; i++) {
//			int cnt = question_cnt-i;
			int cnt = i+1;
			
			HashMap<String, Object> queMap = new HashMap<String, Object>();
			queMap.put("rownum", cnt);
			
			
			double rank = 0;
			for(HashMap<String, Object> oneTestRanking : oneTestRankingList) {
				
				int testScope = Integer.parseInt(oneTestRanking.get("cnt").toString());
				if(cnt < testScope) {
					rank++;
				}
			}
			/*나보다 높은사람수 + 나포함*/
			rank++;
			
			double showTestCnt = oneTestRankingList.size();
			showTestCnt++;

			System.out.println("나보다 높은사람수(나포함) : "+rank);
			System.out.println("시험본사람(나포함+1) : "+showTestCnt);
			
			
			double percentage = 100-(rank/showTestCnt)*100; //14.55
			System.out.println("percentage : "+percentage);
			
			
			/*z core 구하기*/
			double zScore = this.makeZcore(percentage/100);
			System.out.println("zScore : "+zScore);
			
			
			/*최종 IQ 구하기*/
			double iq = Math.round(100+zScore*15+premium);
			queMap.put("score", iq);
			
			resList.add(queMap);
			
		}
		
		
		return resList;
	}
	
	
}
