package com.kb.api.service.iq;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.kb.api.persistence.MasterDao;
import com.kb.api.service.point.PointService;
@Service
public class IqTestService {
	@Inject MasterDao MasterDao;
	@Inject PointService pointService;
	@Value("#{props['point.test']}") private int point_test;
	@Value("#{props['point.test.use']}") private int point_test_use;
	private static final Logger logger = LoggerFactory.getLogger(IqTestService.class);
	 
	
	
	@Transactional(value="transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> result_test_add(HashMap<String, Object> paramMap, Map<String, Object> map, HttpSession session)  throws Exception{
		
		
		/**
		 * 작성자가 member라면 포인트 적립하기
		 */ 
		HashMap<String, Object> retMap = null; 
		retMap = (HashMap<String, Object>) MasterDao.dataReadTra("mapper.IqTestMapper", "test_read", paramMap);
		
		if(retMap != null) {
			String write_type = retMap.get("write_type").toString();
			if(write_type.equals("M") && StringUtils.isEmpty(paramMap.get("member_seq"))) {
				throw new Exception("ERROR_LOGIN");
			}
		}
		
		
		int record = MasterDao.dataCreateTra("mapper.IqTestMapper", "result_test_add", paramMap);
		HashMap<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("result_seq", paramMap.get("result_seq"));
		map.put("data", resMap);
		
		
		if(record > 0) {
			if(retMap != null) {
				String write_type = retMap.get("write_type").toString();
				if(!StringUtils.isEmpty(write_type) && write_type.equals("M")) {
					String write_seq = retMap.get("write_seq").toString();
					
					
					/**
					 * 잔액체크
					 */
					HashMap<String, Object> pointMap = new HashMap<String, Object>();
					pointMap.put("member_seq", paramMap.get("member_seq"));
					pointMap.put("point", point_test_use);
					int chk = MasterDao.dataCount("mapper.LoginMapper", "chk_point", pointMap);
					
					
					if(chk > 0) {
						pointMap.put("member_seq", write_seq);
						pointMap.put("type", "P");
						pointMap.put("content", "UCT응시");
						pointMap.put("point", point_test);
						record = pointService.pointUpdate(pointMap, session);
						
						
						pointMap.put("member_seq", paramMap.get("member_seq"));
						pointMap.put("type", "M");
						pointMap.put("content", "UCT응시");
						pointMap.put("point", point_test_use);
						record += pointService.pointUpdate(pointMap, session);
						
						
						if(record == 2) {
							map.put("success", true);
						}
						
						
					}else {
						throw new Exception("NOTPOINT");
					}
				}else {
					map.put("success", true);
				}
				
			}
		}
		
		return map;
	}
}
