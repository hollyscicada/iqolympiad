package com.kb.api.service.login;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kb.api.persistence.MasterDao;
import com.kb.api.service.point.PointService;
import com.kb.api.util.AES256Util;
import com.kb.api.util.Common;


@Service
public class LoginService {
	@Inject MasterDao MasterDao; 
	@Value("#{props['aes.key']}") private String aes_key;
	@Inject PointService pointService;
	@Value("#{props['point.sign']}") private int point_sign;
	 
	
	public String login(String login_code, String login_type, HttpSession session) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap();
		paramMap.put("login_type", login_type);
		paramMap.put("login_code", login_code);
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) MasterDao.dataRead("mapper.LoginMapper", "login", paramMap);
		
		
		
		if(retMap == null) {
			AES256Util aes = new AES256Util(aes_key);
			login_type = URLEncoder.encode(aes.aesEncode(login_type),"UTF-8");
			login_code = URLEncoder.encode(aes.aesEncode(login_code),"UTF-8");
			return "redirect:/sign?login_type="+login_type+"&login_code="+login_code; 
			
		}else {
			paramMap.put("member_seq", retMap.get("member_seq"));
			this.commonMakeSession(retMap, map, paramMap, session);
			return "common/autoLogin";
		}
	}
	
	
	public Map<String, Object> sign(HashMap paramMap, Map<String, Object> map, HttpSession session) throws Exception{
		AES256Util aes = new AES256Util(aes_key);
		String login_type = URLDecoder.decode(aes.aesDecode(paramMap.get("login_type").toString()), "UTF-8");
		String login_code = URLDecoder.decode(aes.aesDecode(paramMap.get("login_code").toString()), "UTF-8");
		paramMap.put("login_type", login_type);
		paramMap.put("login_code", login_code);
		
		int record = MasterDao.dataCount("mapper.LoginMapper", "sign_cnt", paramMap);
		if(record > 0) {
			map.put("errorMsg", "중복되는 ID가 존재합니다.");
		}else {
			record = MasterDao.dataCreate("mapper.LoginMapper", "sign", paramMap);
			if(record > 0) {
				
				/**
				 * 포인트 추가
				 */
				HashMap<String, Object> pointMap = new HashMap<String, Object>();
				pointMap.put("member_seq", paramMap.get("member_seq"));
				pointMap.put("type", "P");
				pointMap.put("content", "회원가입");
				pointMap.put("point", point_sign);
				
				record = pointService.pointUpdate(pointMap, session);
				
				if(record > 0) {
					HashMap<String, Object> retMap = null;
					retMap = (HashMap) MasterDao.dataRead("mapper.LoginMapper", "user_info_seq", paramMap);
					if (retMap != null) {
						commonMakeSession(retMap, map, paramMap, session);
						map.put("success", true);
					}
				}
			}
		}
		return map;
	}
	
	
	public Map<String, Object> commonMakeSession(HashMap<String, Object> retMap, Map<String, Object> map,  HashMap paramMap, HttpSession session) {
		
		retMap = (HashMap) MasterDao.dataRead("mapper.LoginMapper", "user_info_seq", paramMap);
		retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
		retMap.put("custom_type", retMap.get("custom_type"));
		retMap.put("custom_code", retMap.get("custom_code"));
		session.setAttribute("MEMBER", retMap);
		
		map.put("authToken", retMap.get("authToken"));
		map.put("custom_type", retMap.get("custom_type"));
		map.put("custom_code", retMap.get("custom_code"));
		map.put("success", true);
		
		return map;
	}
	public void commonMakeSession(HashMap<String, Object> paramMap, HttpSession session) {
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		retMap = (HashMap) MasterDao.dataReadTra("mapper.LoginMapper", "user_info_seq", paramMap);
		retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
		retMap.put("custom_type", retMap.get("custom_type"));
		retMap.put("custom_code", retMap.get("custom_code"));
		session.setAttribute("MEMBER", retMap);
	}
}
