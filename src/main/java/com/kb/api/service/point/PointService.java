package com.kb.api.service.point;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.kb.api.persistence.MasterDao;
import com.kb.api.service.login.LoginService;
@Service
public class PointService {
	@Inject MasterDao MasterDao;
	@Inject LoginService loginService;
	private static final Logger logger = LoggerFactory.getLogger(PointService.class);
	 
	public int pointUpdate(HashMap<String, Object> pointMap, HttpSession session)  throws Exception{
		int record = MasterDao.dataUpdateTra("mapper.LoginMapper", "point_update", pointMap);
		
		if(record > 0) {
			loginService.commonMakeSession(pointMap, session);
		}
		
		try {
			MasterDao.dataCreateTra("mapper.PointMapper", "point_history", pointMap);
		}catch(Exception e) {
			logger.error("(포인트)로그저장 에러 = {}", pointMap);
		}
		
		return record;
	}
	
}
