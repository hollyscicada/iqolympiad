package com.kb.api.service.member;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.kb.api.persistence.MasterDao;
import com.kb.api.service.main.MainService;
import com.kb.api.util.Util;
@Service
public class MemberService {
	@Inject MasterDao MasterDao;
	@Inject MainService mainService;
	
	public String commonChkSession(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		return null;
	}
	 
	/**
	 * 세션에 값 잇는지 체크후에 없다면 로그인페이지로 리다이렉트
	 * @param session
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String chkSession(HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		if(sessionMap == null) {
			String redirect_url = URLEncoder.encode(request.getRequestURI(), "UTF-8");
			session.setAttribute("redirect_url", redirect_url);
			return "redirect:/login?redirect_url="+redirect_url;
		}
		return null;
	}
}
