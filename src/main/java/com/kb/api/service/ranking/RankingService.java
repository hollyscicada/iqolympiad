package com.kb.api.service.ranking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.kb.api.persistence.MasterDao;
@Service
public class RankingService {
	@Inject MasterDao MasterDao;
	 
	public Map<String, Object> ranking(HashMap<String, Object> paramMap, Map<String, Object> map)  throws Exception{
		
		List<HashMap<String, Object>> todayList = (List) MasterDao.dataList("mapper.RankingMapper", "today_ranking", paramMap);
		List<HashMap<String, Object>> yesterdayList = (List) MasterDao.dataList("mapper.RankingMapper", "yesterday_ranking", paramMap);
		 
		
		for(HashMap<String, Object> todayMap : todayList) {
			
			int today_member_seq = Integer.parseInt(todayMap.get("member_seq").toString());
			long today_ranking = Math.round(Double.parseDouble(todayMap.get("rownum").toString()));
			todayMap.put("rownum", today_ranking);
			
			
			String change_type = "CHK";
			long change_ranking = 0;  
			for(HashMap<String, Object> yesterMap : yesterdayList) { 
				int yesterday_member_seq = Integer.parseInt(yesterMap.get("member_seq").toString());
				long yesterday_ranking = Math.round(Double.parseDouble(yesterMap.get("rownum").toString()));
				
				
				if(today_member_seq == yesterday_member_seq) { 
					 
					/**랭킹이 떨어짐*/
					if(today_ranking > yesterday_ranking) {
						change_type = "DOWN";
						change_ranking = today_ranking-yesterday_ranking;
					}
					
					/**랭킹이 올라감*/
					if(today_ranking < yesterday_ranking) {
						change_type = "UP";
						change_ranking = yesterday_ranking-today_ranking;
					}
				}
			}
			todayMap.put("change_type", change_type);
			todayMap.put("change_ranking", change_ranking);
			
		}
		map.put("data", todayList);
		return map;
		
	}
	
}
