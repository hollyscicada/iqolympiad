package com.kb.api.util;

import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;

@Component
public class MailSendUtil {
	
	private JavaMailSenderImpl mailSender;

	@Value("#{props['user.mail.send.address']}")
	public String sender; 
	@Value("#{props['spring.mail.host']}")
	public String host;
	@Value("#{props['spring.mail.port']}")
	public String port;
	@Value("#{props['spring.mail.username']}")
	public String username;
	@Value("#{props['spring.mail.password']}")
	public String password;
	@Value("#{props['spring.mail.properties.mail.smtp.auth']}")
	public String auth;
	@Value("#{props['spring.mail.properties.mail.smtp.starttls.enable']}")
	public String starttls;
	
	public void mailSend(Map<String, String> dataMap) throws MessagingException, javax.mail.MessagingException 
	{		
		mailSender = new JavaMailSenderImpl();
		mailSender.setHost(host);
		mailSender.setPort(Integer.parseInt(port));
		mailSender.setUsername(username);
		mailSender.setPassword(password);
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", auth);
		properties.setProperty("mail.smtp.starttls.enable", starttls);
		mailSender.setJavaMailProperties(properties);
		
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
		mimeMessage.setContent(dataMap.get("CONTENTS"), "text/html");
		messageHelper.setTo(dataMap.get("TARGET_ADDRESS"));
		messageHelper.setFrom(sender);
		messageHelper.setSubject(dataMap.get("TITLE"));
		messageHelper.setText(dataMap.get("CONTENTS"), true);
		
		mailSender.send(message);
	}
}
