package com.kb.api.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

public class ErrorCode {
	
    
	public static String errorMap(String key) {
		HashMap<String, String> map = new HashMap();
		map.put("900", "해당테스트의 문항이 없습니다.");
		map.put("901", "해당테스트의 문항이 Integer형식이 아닙니다.");
		
		
		return map.get(key);
	}
	public static String find(String key) {
		return errorMap(key);
	}
	
	public static Map<String, Object> makeError(Map<String, Object> map, Exception e){
		if(!StringUtils.isEmpty(ErrorCode.find(e.getMessage()))) {
			map.put("errorCode", e.getMessage());
			map.put("errorMsg", ErrorCode.find(e.getMessage()));
		}else {
			e.printStackTrace();
			map.put("errorCode", "500");
			map.put("errorMsg", e.getMessage());
		}
		return map;
	}
}
