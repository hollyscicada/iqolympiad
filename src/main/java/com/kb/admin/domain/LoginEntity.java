package com.kb.admin.domain;

public class LoginEntity extends CommonEntity{
	private String admin_seq;
	private String id;
	private String pw;
	private String name;
	private String phone;
	private String promotion_code;
	private String role;
	private String role_text;
	
	 
	public String getPromotion_code() {
		return promotion_code;
	}
	public void setPromotion_code(String promotion_code) {
		this.promotion_code = promotion_code;
	}
	public String getRole_text() {
		return role_text;
	}
	public void setRole_text(String role_text) {
		this.role_text = role_text;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAdmin_seq() {
		return admin_seq;
	}
	public void setAdmin_seq(String admin_seq) {
		this.admin_seq = admin_seq;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
	
	
	
}
