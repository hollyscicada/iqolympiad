package com.kb.admin.domain;

public class MemberEntity extends CommonEntity{

	
	private String member_seq;
	private String login_type;
	private String email;
	private String name;
	private String sex;
	private String age;
	private String country_seq;
	private String country_name;
	private String point;
	private String iq;
	private String img_seq;
	
	
	
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getMember_seq() {
		return member_seq;
	}
	public void setMember_seq(String member_seq) {
		this.member_seq = member_seq;
	}
	public String getLogin_type() {
		return login_type;
	}
	public void setLogin_type(String login_type) {
		this.login_type = login_type;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCountry_seq() {
		return country_seq;
	}
	public void setCountry_seq(String country_seq) {
		this.country_seq = country_seq;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getIq() {
		return iq;
	}
	public void setIq(String iq) {
		this.iq = iq;
	}
	public String getImg_seq() {
		return img_seq;
	}
	public void setImg_seq(String img_seq) {
		this.img_seq = img_seq;
	}
	
	
	
	
	
	
	
	
	
	
}
