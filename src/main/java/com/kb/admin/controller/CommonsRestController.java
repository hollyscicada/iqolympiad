package com.kb.admin.controller;

import java.util.HashMap;

import javax.inject.Inject;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;

@RestController
public class CommonsRestController {
	
	@Inject MasterService masterService;
	
	
	/**
	 * 관리자 노출비노출 
	 * @param model
	 * @param table_name
	 * @param seq_name
	 * @param seq
	 * @param show_yn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/supervise/api/v1/show/{table_name}/{seq_name}/{seq}/{show_yn}"} , method = RequestMethod.GET)
	public @ResponseBody String CommonCodeUpdateGET(Model model,@PathVariable String table_name,@PathVariable String seq_name,@PathVariable int seq,@PathVariable String show_yn) throws Exception {
		HashMap<String, Object> map = new HashMap<>();
		map.put("table_name", table_name);
		map.put("seq_name", seq_name);
		map.put("seq", seq);
		map.put("show_yn", show_yn);
		
		String rst = "N";
		int record = masterService.dataUpdate("mapper.admin.AdminMapper","showyn",map);
		if(record > 0) {
			rst = "Y";
		}
		
		return rst;
	}
}
