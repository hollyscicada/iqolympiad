package com.kb.admin.controller;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kb.admin.domain.AdminEntity;
import com.kb.admin.domain.LoginEntity;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;


@Controller
public class LoginsController {
	@Inject MasterService masterService;

	private static final Logger logger = LoggerFactory.getLogger(LoginsController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws Exception 
	 */
	/*관리자 현황판*/
	@RequestMapping(value ={ "/supervise" }, method = RequestMethod.GET)
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		String loginCheck = (String) session.getAttribute("admin_seq");
		if(loginCheck != null && !"".equals(loginCheck)) {   
			return "adminLayout/admin/index";
		}else {
			return "adminLayoutNotLogin/admin/login";
		}
	}   
	/**
	 * @msg : 로그아웃
	 */
	@RequestMapping(value = {"/supervise/logout"}, method =  { RequestMethod.GET , RequestMethod.POST })
	public String logout(Model model, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		session.invalidate();
		return "redirect:/supervise";
	}
}
